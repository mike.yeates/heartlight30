﻿mergeInto(LibraryManager.library, {

	SetNumHeartsCollected: function (numHeartsCollected) {
		heartsCollectedLabel.textContent = numHeartsCollected;
	},

	SetTotalHearts: function (totalHearts) {
		totalHeartsLabel.textContent = totalHearts;
		totalHeartsLabel.classList.remove("disabled");
	},

	SetCurrentLevel: function (currentLevel) {
		currentLevelLabel.textContent = currentLevel;
	},

	SetTotalLevels: function (totalLevels) {
		totalLevelsLabel.textContent = totalLevels;
	},

	DisableTotalHeartsLabel: function () {
		totalHeartsLabel.classList.add("disabled");
	},

	SetHeaderColour: function (headerColour) {
		
		header.classList.remove("headerBackgroundBrown");
		header.classList.remove("headerBackgroundBurgundy");
		header.classList.remove("headerBackgroundPurple");
		header.classList.remove("headerBackgroundViolet");

		gameUIText.classList.remove("headerTextBrown");
		gameUIText.classList.remove("headerTextBurgundy");
		gameUIText.classList.remove("headerTextPurple");
		gameUIText.classList.remove("headerTextViolet");

		switch(headerColour) {
			case 0:
				// Brown
				header.classList.add("headerBackgroundBrown");
				gameUIText.classList.add("headerTextBrown");
				break;
			case 1:
				// Burgundy
				header.classList.add("headerBackgroundBurgundy");
				gameUIText.classList.add("headerTextBurgundy");
				break;
			case 2:
				// Purple
				header.classList.add("headerBackgroundPurple");
				gameUIText.classList.add("headerTextPurple");
				break;
			case 3:
				// Violet
				header.classList.add("headerBackgroundViolet");
				gameUIText.classList.add("headerTextViolet");
				break;
			case 4:
				// No bricks, don't append any classes so default colour is used
				break;
			default:
				// Something weird happened, use burgundy
				header.classList.add("headerBackgroundBurgundy");
				gameUIText.classList.add("headerTextBurgundy");
		} 
	}
});