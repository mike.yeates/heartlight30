﻿mergeInto(LibraryManager.library, {

	LogLevelCompleted: function (levelID) {

		gtag('event',
			 'LevelComplete', {
			 'event_category': 'LevelProgression',
			 'event_label': (levelID + 1)
		});
	},

	LogPlayerDied: function (levelID) {
		
		gtag('event',
			 'PlayerDied', {
			 'event_category': 'LevelProgression',
			 'event_label': (levelID + 1)
		});
	}
	
});