﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class PostBuildActions
{
    [PostProcessBuild]
    public static void OnPostProcessBuild(BuildTarget target, string targetPath)
    {
        // Remove default mobile warning as as custom message is used instead
        // https://gist.github.com/JohannesDeml/f551b1e60c59e8472c3e843014d7bd10
        /*if (target != BuildTarget.WebGL)
        {
            return;
        }

        var buildFolderPath = Path.Combine(targetPath, "Build");
        var info = new DirectoryInfo(buildFolderPath);
        var files = info.GetFiles("*.js");
        for (int i = 0; i < files.Length; i++)
        {
            var file = files[i];
            var filePath = file.FullName;
            var text = File.ReadAllText(filePath);
            text = text.Replace("UnityLoader.SystemInfo.mobile", "false");

            Debug.Log("Removing iOS warning from " + filePath);
            File.WriteAllText(filePath, text);
        }
        */
    }
}
