﻿Shader "Custom/Sprite (Noise Overlay)"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0

        [Header(Offset)]
        _OffsetX ("X", Float) = 0
        _OffsetY ("Y", Float) = 0

        [Header(Noise Overlay)]
		[NoScaleOffset]
		_NoiseTex ("Noise (RGB)", 2D) = "gray" {}
		_NoiseFrequency ("Noise Frequency", Range(0,1)) = 0.5
        _NoiseOffsetX ("Noise Offset X", Float) = 0
        _NoiseOffsetY ("Noise Offset Y", Float) = 0
        _NoiseOctaves ("Noise Octaves", Range(0,5)) = 1
		_BrightnessBalance ("Brightness Balance", Range(0,1)) = 0.5
		_OverlayIntensity ("Overlay Intensity", Range(0,1)) = 0.5
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
            };

            fixed4 _Color;
            float _OffsetX;
            float _OffsetY;

            // Overlay
            sampler2D _NoiseTex;
		    float _NoiseFrequency;
            float _NoiseOffsetX;
            float _NoiseOffsetY;
            float _NoiseOctaves;
		    float _BrightnessBalance;
		    float _OverlayIntensity;

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord + float2(_OffsetX, _OffsetY);
                OUT.color = IN.color * _Color;

                // To sample overlay texture in world space
                OUT.worldPos = mul(unity_ObjectToWorld, IN.vertex);

                #ifdef PIXELSNAP_ON
                    OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }
            sampler2D _MainTex;
            sampler2D _AlphaTex;
            float _AlphaSplitEnabled;

            fixed4 SampleSpriteTexture (float2 uv)
            {
                fixed4 color = tex2D (_MainTex, uv);

                #if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
                    if (_AlphaSplitEnabled)
                    color.a = tex2D (_AlphaTex, uv).r;
                #endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

                return color;
            }

            float SampleNoise(sampler2D noiseTex, float2 uvs, int octaves, float persistence)
            {
                float total = 0;
                float amplitude = 1;
                float maxValue = 0; // Used for normalising result to 0-1

                float4 rawNoise = tex2D(_NoiseTex, uvs);

                // Can only use constants for loop counters in older WebGL
                for(int i = 0; i < 4; i++)
                {
                    total += rawNoise[i] * amplitude;
                    maxValue += amplitude;
                    amplitude *= persistence;
                    // Frequency is already doubled in next channel of texture
                }

                return total / maxValue;
	    	}

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;

                float2 noiseUVs = (IN.worldPos.xy + float2(_NoiseOffsetX, _NoiseOffsetY)) * _NoiseFrequency;
                float noise = SampleNoise(_NoiseTex, noiseUVs, _NoiseOctaves, 1.5f);
			    noise = (noise - (1 - _BrightnessBalance)) * _OverlayIntensity;

                c.rgb += noise;
                c.rgb *= c.a;
                return c;
            }
            ENDCG
        }
    }
}