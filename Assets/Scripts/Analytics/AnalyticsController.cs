﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices; // To call JS functions from C# (update UI text)

public class AnalyticsController : MonoBehaviour
{
    // Declare external JS functions
    [DllImport("__Internal")]
    private static extern void LogLevelCompleted(int levelID);
    [DllImport("__Internal")]
    private static extern void LogPlayerDied(int levelID);

    void OnEnable()
    {
        Level.levelCompleteDelegate += OnLevelComplete;
        Player.playerDiedDelegate += OnPlayerDead;
    }

    void OnDisable()
    {
        Level.levelCompleteDelegate -= OnLevelComplete;
        Player.playerDiedDelegate -= OnPlayerDead;
    }

#region Events
    public void OnLevelComplete(int levelID)
    {
        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            LogLevelCompleted(levelID);
        }
    }

    public void OnPlayerDead(int levelID)
    {
        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            LogPlayerDied(levelID);
        }
    }
#endregion

}
