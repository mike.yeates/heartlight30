﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controls animations which use a material offset
public class AmbientAnimationController : MonoBehaviour
{
    [Header("Plasma")]
    public Material plasmaMat;
    public Vector2 plasmaAnimationSpeed;
    private Vector2 currentPlasmaOffset;

    [Header("Warp Left")]
    public Material warpLeftMat;
    public Vector2 warpLeftAnimationSpeed;
    private Vector2 currentWarpLeftOffset;

    [Header("Warp Right")]
    public Material warpRightMat;
    public Vector2 warpRightAnimationSpeed;
    private Vector2 currentWarpRightOffset;

    [Header("Grass Ovelay")]
    public Material grassMat;
    public Vector2 grassOverlayAnimationSpeed;
    private Vector2 currentGrassOverlayOffset;

    [Header("Balloon String")]
    public Material ropeMat;
    public float ropeAnimationSpeed;
    private float currentRopeOffset;

    void OnEnable()
    {
        GameManager.levelChangeDelegate += OnLevelChange;
        NoiseWobble.allowedToUpdate = true;
    }

    void OnDisable()
    {
        GameManager.levelChangeDelegate -= OnLevelChange;
        NoiseWobble.allowedToUpdate = false;
    }

    void Start()
    {
        if(plasmaMat)
        {
            plasmaMat.SetFloat("_OffsetX", 0.0f);
            plasmaMat.SetFloat("_OffsetY", 0.0f);
        }

        if(warpLeftMat)
        {
            warpLeftMat.SetFloat("_OffsetX", 0.0f);
            warpLeftMat.SetFloat("_OffsetY", 0.0f);
        }

        if(warpRightMat)
        {
            warpRightMat.SetFloat("_OffsetX", 0.0f);
            warpRightMat.SetFloat("_OffsetY", 0.0f);
        }

        if(grassMat)
        {
            grassMat.SetFloat("_NoiseOffsetX", 0.0f);
            grassMat.SetFloat("_NoiseOffsetY", 0.0f);
        }

        if(ropeMat)
        {
            ropeMat.SetFloat("_OffsetY", 0.0f);
        }
    }

    void Update()
    {
        if(plasmaMat)
        {
            currentPlasmaOffset += plasmaAnimationSpeed * Time.deltaTime;
            currentPlasmaOffset = WrapOffset(currentPlasmaOffset);
            plasmaMat.SetFloat("_OffsetX", currentPlasmaOffset.x);
            plasmaMat.SetFloat("_OffsetY", currentPlasmaOffset.y);
        }

        if(warpLeftMat)
        {
            currentWarpLeftOffset += warpLeftAnimationSpeed * Time.deltaTime;
            currentWarpLeftOffset = WrapOffset(currentWarpLeftOffset);
            warpLeftMat.SetFloat("_OffsetX", currentWarpLeftOffset.x);
            warpLeftMat.SetFloat("_OffsetY", currentWarpLeftOffset.y);
        }

        if(warpRightMat)
        {
            currentWarpRightOffset += warpRightAnimationSpeed * Time.deltaTime;
            currentWarpRightOffset = WrapOffset(currentWarpRightOffset);
            warpRightMat.SetFloat("_OffsetX", currentWarpRightOffset.x);
            warpRightMat.SetFloat("_OffsetY", currentWarpRightOffset.y);
        }

        if(grassMat)
        {
            currentGrassOverlayOffset += grassOverlayAnimationSpeed * Time.deltaTime;
        
            grassMat.SetFloat("_NoiseOffsetX", currentGrassOverlayOffset.x);
            grassMat.SetFloat("_NoiseOffsetY", currentGrassOverlayOffset.y);
        }

        if(ropeMat)
        {
            currentRopeOffset += ropeAnimationSpeed * Time.deltaTime;
            currentRopeOffset = WrapOffset(currentRopeOffset);
            ropeMat.SetFloat("_OffsetY", currentRopeOffset);
        }

    }

    public void OnLevelChange(Level level)
    {
        // Randomise noise offset for grass overlay
        currentGrassOverlayOffset = new Vector2(Random.Range(0.0f, 100.0f), Random.Range(0.0f, 100.0f));
    }

#region Helpers
    private Vector2 WrapOffset(Vector2 offset)
    {
        // Wrap x and y between 0-1
        offset.x = WrapOffset(offset.x);
        offset.y = WrapOffset(offset.y);

        return offset;
    }

    private float WrapOffset(float offset)
    {
        if(offset > 1.0f)
        {
            offset -= 1.0f;
        }
        else if(offset < 0.0f)
        {
            offset += 1.0f;
        }
        return offset;
    }
#endregion
}
