﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseWobble : MonoBehaviour
{
    public float wobbleSpeed;
    public float maxMoveDistance; // How far object is allowed to move on the X axis relative to local origin
    private float currentWobbleTime;

    public static bool allowedToUpdate;

    void Start()
    {
        // Random initial offset
        currentWobbleTime = Random.Range(0.0f, 100.0f);
    }

    void Update()
    {
        if(allowedToUpdate)
        {
              currentWobbleTime += Time.deltaTime * wobbleSpeed;
        
            // Re-map noise from 0-1 range into -1.0 to 1.0 range, and scale by maxMoveDistance
            float offset = (Mathf.PerlinNoise(currentWobbleTime, 0.0f) * 2.0f - 1.0f) * maxMoveDistance;

            transform.localPosition = new Vector3(offset,
                                                transform.localPosition.y,
                                                transform.localPosition.z);
        }
    }
}
