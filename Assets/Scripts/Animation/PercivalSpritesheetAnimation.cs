﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Animation frames within the spritesheet (in order from left to right)
public enum PercivalAnimationFrame
{
    IdleRight,
    IdleLeft,
    WalkRight,
    WalkLeft,
    CelebrateGround,
    CelebrateJump,
    BlockEarsRight,
    BlockEarsLeft,
    Count // Helper to count num frames
};

// Handles horizontal spritesheet animation for the player
public class PercivalSpritesheetAnimation : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private Material percivalMaterial;
    private float frameWidth;

    private Vector2Int previousPosition; // To determine change in direction
    private PercivalAnimationFrame currentFrame;
    private Direction currentDirectionPlayerIsFacing;

    // Special animation states
    private bool isJumping;
    private bool isBlockingEars;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        percivalMaterial = spriteRenderer.sharedMaterial;
        frameWidth = 1.0f / (int)PercivalAnimationFrame.Count;

        // Ensure material is tiled so only one frame is shown at a time
        percivalMaterial.SetFloat("_TilingX", frameWidth);
    }

    public void Init()
    {
        SwapToFrame(PercivalAnimationFrame.IdleRight);
        currentDirectionPlayerIsFacing = Direction.E;

        isJumping = false;
        isBlockingEars = false;
    }

    public void StepAnimation(Direction latestInput, Vector2Int pos)
    {
        // Called by Player at the end of each physics step (8 times per second)

        if(isBlockingEars)
        {
            SwapToFrame(GetBlockEarsCycle());
            return;
        }
        else if(isJumping)
        {
            SwapToFrame(GetJumpingCycle());
            return;
        }

        bool didPlayerMove = pos != previousPosition;

        if(latestInput == Direction.E || latestInput == Direction.W)
            currentDirectionPlayerIsFacing = latestInput;

        // If we received keyboard input
        if(latestInput != Direction.None)
        {
            if(didPlayerMove)
            {
                // If input and player did move, toggle between idle and walk frames
                SwapToFrame(GetWalkCycleForDirection(latestInput));
            }
            else
            {
                // If input and player didn't move (walking into a wall etc.)
                // Ensure idle sprite is facing correct direction
                SwapToFrame(GetIdleSpriteForDirection(latestInput));  
            }
        }
        else
        {
            // No keyboard input, swap to idle sprite
            SwapToFrame(GetIdleSpriteForDirection(currentDirectionPlayerIsFacing));
        }

        // Remember position for next step to determine if a movement ocured
        previousPosition = pos;
        
    }

    private void SwapToFrame(PercivalAnimationFrame targetFrame)
    {
        // Cast enum to an int to work out which frame we want 
        int targetFrameNumber = (int)targetFrame;

        // Offset UVs in shader to show a new section of the spritesheet   
        percivalMaterial.SetFloat("_OffsetX", targetFrameNumber * frameWidth);

        currentFrame = targetFrame;
    }

    public void PlayBlockEarsSequence()
    {
        isBlockingEars = true;
    }

    public void PlayJumpingSequence()
    {
        isJumping = true;
    }

#region Helpers
    private PercivalAnimationFrame GetBlockEarsCycle()
    {
        // Toggles between left and right ear block pose
        return currentFrame == PercivalAnimationFrame.BlockEarsLeft ? PercivalAnimationFrame.BlockEarsRight : PercivalAnimationFrame.BlockEarsLeft;
    }

    private PercivalAnimationFrame GetJumpingCycle()
    {
        // Toggles between in the air and on the ground jump pose
        return currentFrame == PercivalAnimationFrame.CelebrateJump ? PercivalAnimationFrame.CelebrateGround : PercivalAnimationFrame.CelebrateJump;
    }

    private PercivalAnimationFrame GetWalkCycleForDirection(Direction dir)
    {
        // Toggles between idle and walk pose each time it is called
        if(currentFrame == PercivalAnimationFrame.IdleLeft ||
           currentFrame == PercivalAnimationFrame.IdleRight)
        {
            // Currently on an idle pose, so return a walk pose
            return GetWalkSpriteForDirection(dir);
        }
        else
        {
            // Currently on a walk pose, so return an idle pose
            return GetIdleSpriteForDirection(dir);
        }
    }

    private PercivalAnimationFrame GetIdleSpriteForDirection(Direction dir)
    {   
        if(dir == Direction.N || dir == Direction.S)
        {
            // When moving up and down, ensure we maintain current direction player is already facing
            dir = currentDirectionPlayerIsFacing;
        }

        return dir == Direction.W ? PercivalAnimationFrame.IdleLeft : PercivalAnimationFrame.IdleRight;
    }

    private PercivalAnimationFrame GetWalkSpriteForDirection(Direction dir)
    {   
        if(dir == Direction.N || dir == Direction.S)
        {
            // When moving up and down, ensure we maintain current direction player is already facing
            dir = currentDirectionPlayerIsFacing;
        }

        return dir == Direction.W ? PercivalAnimationFrame.WalkLeft : PercivalAnimationFrame.WalkRight;
    }
#endregion

}
