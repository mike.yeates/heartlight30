﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Animation frames within the spritesheet (in order from left to right)
public enum ExplosionAnimationFrame
{
    VeryLarge,
    Large,
    Medium,
    Small,
    Shrapnel,
    Blank,
    Count // Helper to count num frames in spritesheet
};

// Handles horizontal spritesheet animation for explosions
public class ExplosionSpritesheetAnimation : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    private Material explosionMaterial;
    private float frameWidth;

    private ExplosionAnimationFrame currentFrame;

    // Define animation sequences (each index is a step of the animation)
    private ExplosionAnimationFrame[] frameSquence = { ExplosionAnimationFrame.Small,
                                                       ExplosionAnimationFrame.Medium, 
                                                       ExplosionAnimationFrame.Large,
                                                       ExplosionAnimationFrame.VeryLarge,
                                                       ExplosionAnimationFrame.Large,
                                                       ExplosionAnimationFrame.Medium, 
                                                       ExplosionAnimationFrame.Small,
                                                       ExplosionAnimationFrame.Blank // Only used if we go out of range
                                                      };

    private ExplosionAnimationFrame[] frameSquenceShrapnel = { ExplosionAnimationFrame.Shrapnel,
                                                               ExplosionAnimationFrame.Medium, 
                                                               ExplosionAnimationFrame.Large,
                                                               ExplosionAnimationFrame.VeryLarge,
                                                               ExplosionAnimationFrame.Large,
                                                               ExplosionAnimationFrame.Medium, 
                                                               ExplosionAnimationFrame.Small,
                                                               ExplosionAnimationFrame.Blank
                                                              };

    public void Awake()
    {
        explosionMaterial = spriteRenderer.material; // Create instance of material
        frameWidth = 1.0f / (int)ExplosionAnimationFrame.Count;

        // Ensure material is tiled so only one frame is shown at a time
        explosionMaterial.SetFloat("_TilingX", frameWidth);
    }

    public void Init()
    {
        SwapToFrame(ExplosionAnimationFrame.Small);
    }

    public void StepAnimation(bool wasBomb, int frameNum)
    {
        // Show a blank frame if index is out of range
        if(frameNum < 0 || frameNum >= frameSquence.Length)
            frameNum = frameSquence.Length - 1;

        // Called by Player at the end of each physics step (8 times per second)
        if(wasBomb)
        {
            SwapToFrame(frameSquenceShrapnel[frameNum]);
        }
        else
        {
            SwapToFrame(frameSquence[frameNum]);
        }
    }

    private void SwapToFrame(ExplosionAnimationFrame targetFrame)
    {
        // Cast enum to an int to work out which frame we want 
        int targetFrameNumber = (int)targetFrame;

        if(explosionMaterial != null)
        {
            // Offset UVs in shader to show a new section of the spritesheet   
            explosionMaterial.SetFloat("_OffsetX", targetFrameNumber * frameWidth);
        }
       
        currentFrame = targetFrame;
    }

}
