﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Animates an object towards a target position
public class SlideObject : MonoBehaviour
{
    private Vector3 targetPosition;
    private bool isAnimating;
    private float animationSpeed;

    void Start()
    {
        isAnimating = false;
    }

    void Update()
    {
        if(isAnimating)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * animationSpeed);

            if(Vector3.Distance(transform.position, targetPosition) <= 0.1f)
            {
                isAnimating = false;
            }
        }
    }

    public void SlideToPosition(Vector3 target, float duration)
    {
        animationSpeed = 1.0f / duration;
        targetPosition = target;
        isAnimating = true;
    }
}
