﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineWaveWobble : MonoBehaviour
{
    public float wobbleSpeed;
    public float maxMoveDistance; // How far object is allowed to move on the X axis relative to local origin
    private float currentWobbleTime;

    void Start()
    {
        
    }

    void Update()
    {
        currentWobbleTime += Time.deltaTime * wobbleSpeed;
        
        float offset = Mathf.Sin(currentWobbleTime) * maxMoveDistance;

        transform.localPosition = new Vector3(offset,
                                              transform.localPosition.y,
                                              transform.localPosition.z);
    }
}
