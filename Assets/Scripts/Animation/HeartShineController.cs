﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartShineController : MonoBehaviour
{
    public float animationSpeed;

    // Seconds of padding between loops of animation
    public float loopDelayMin;
    public float loopDelayMax;

    [Header("Primary Shine Animation")]
    public SpriteRenderer primaryShineSprite;
    public float primaryShineRotateSpeed;
    public AnimationCurve primaryShineAlphaOverTime;

    [Header("Secondary Shine Animation")]
    public SpriteRenderer secondaryShineSprite;
    public float secondaryShineRotateSpeed;
    public AnimationCurve secondaryShineAlphaOverTime;

    // Where we are up to in the animation curves
    private float curveSampleTime;
    
    void Start()
    {
        // Randomise initial rotation
        primaryShineSprite.transform.Rotate(0.0f, 0.0f, Random.Range(0.0f, 180.0f));
        secondaryShineSprite.transform.Rotate(0.0f, 0.0f, Random.Range(0.0f, 180.0f));

        curveSampleTime = Random.value;
    }

    void Update()
    {
        primaryShineSprite.transform.Rotate(0.0f, 0.0f, Time.deltaTime * primaryShineRotateSpeed);
        secondaryShineSprite.transform.Rotate(0.0f, 0.0f, Time.deltaTime * secondaryShineRotateSpeed);

        curveSampleTime += animationSpeed * Time.deltaTime;

        primaryShineSprite.color = new Color(primaryShineSprite.color.r,
                                             primaryShineSprite.color.g,
                                             primaryShineSprite.color.b,
                                             Mathf.Clamp01(primaryShineAlphaOverTime.Evaluate(curveSampleTime)));

        secondaryShineSprite.color = new Color(secondaryShineSprite.color.r,
                                               secondaryShineSprite.color.g,
                                               secondaryShineSprite.color.b,
                                               Mathf.Clamp01(secondaryShineAlphaOverTime.Evaluate(curveSampleTime)));

        if(curveSampleTime > 1.0f)
        {
            // Random delay until next sample (sample is 0-1 clamped so negative is fine)
            curveSampleTime = Random.Range(-loopDelayMax, -loopDelayMin);
        }
    }
}
