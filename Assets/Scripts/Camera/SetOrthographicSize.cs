﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class SetOrthographicSize : MonoBehaviour
{
    private float targetAspectRatio;
    private Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
        targetAspectRatio = (float)Level.LEVEL_WIDTH / (float)Level.LEVEL_HEIGHT;

        FitContentToScreen();
    }

    private void FitContentToScreen()
    {
        // Get world-space pos of bottom left of screen and top right of screen
        Vector3 screenMin = cam.ScreenToWorldPoint(new Vector3(0, 0, cam.nearClipPlane));
        Vector3 screenMax = cam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, cam.nearClipPlane));

        // Work out how many grid tiles can fit on the screen (a grid tile is 1x1 unit)
        float screenWidthTiles = screenMax.x - screenMin.x;
        float screenHeightTiles = screenMax.y - screenMin.y;
       
        // Set orthographicSize so game always fits in window (maintaining aspect ratio)

        // The orthographicSize is half the size of the vertical viewing volume
        // Default is 6 as level is 12 units tall
        // A smaller orth size makes the game appear larger

        // If we fill the width, will game height still fit
        if(screenWidthTiles / targetAspectRatio <= Level.LEVEL_HEIGHT)
        {
            // Fill width
            cam.orthographicSize = Level.LEVEL_WIDTH / screenWidthTiles * screenHeightTiles / 2.0f;
        }
        else
        {
            // Fill height
            //cam.orthographicSize = screenHeightTiles / 2.0f;
            cam.orthographicSize = Level.LEVEL_HEIGHT / 2.0f;
        }
    }
}
