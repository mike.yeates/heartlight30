﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFader : Singleton<CameraFader>
{
    public Texture2D black;

    public const float DEFAULT_FADE_DURATION = 0.75f;
    private float fadeSpeed;
    private bool isFadingToBlack = false;
    private bool isFadingToClear = false;

    private float currentAlpha;

    // Event triggered when screen reaches full black
    public delegate void OnScreenBlackDelegate();
    public static OnScreenBlackDelegate screenBlackDelegate;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    
    void Update()
    {
        if(isFadingToBlack)
        {
            currentAlpha += Time.unscaledDeltaTime * fadeSpeed;

            if(currentAlpha >= 1.0f)
            {
                if (screenBlackDelegate != null)
                    screenBlackDelegate();

                currentAlpha = 1.0f;
                isFadingToBlack = false;
            }
        }
        else if(isFadingToClear)
        {
            currentAlpha -= Time.unscaledDeltaTime * fadeSpeed;

            if(currentAlpha <= 0.0f)
            {
                currentAlpha = 0.0f;
                isFadingToClear = false;
            }
        }
    }

    void OnGUI()
	{
		if (currentAlpha >= 0.0f)
		{
			GUI.color = new Color(1.0f, 1.0f, 1.0f, currentAlpha);
			GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), black);
			GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

    public void FadeToClear(float duration = DEFAULT_FADE_DURATION)
    {
        fadeSpeed = 1.0f / duration;
        isFadingToBlack = false;
        isFadingToClear = true;
    }

    public void FadeToBlack(float duration = DEFAULT_FADE_DURATION)
    {
        fadeSpeed = 1.0f / duration;
        isFadingToClear = false;
        isFadingToBlack = true;
    }

    public void JumpToClear()
    {
        currentAlpha = 0.0f;
    }

    public void JumpToBlack()
    {
        currentAlpha = 1.0f;
    }
}
