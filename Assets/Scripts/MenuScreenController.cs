﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using TMPro;

public enum MenuMode
{
    PauseScreen,
    StartScreen
};

public class MenuScreenController : MonoBehaviour
{
    public RectTransform menuContainer;
    public MaskableGraphic[] foregroundUIElements; // For mass fade in/out
    
    [Header("Sub Screens")]
    public UIAnimation menuTimeline;
    public UIAnimation optionsTimeline;
    public UIAnimation controlsTimeline;

    [Header("Animation")]
    [Range(0.0f, 1.0f)]
    public float uiForegroundOpacity = 0.0f;
    public AmbientAnimationController ambientAnimations; // So we can disable/enable these

    [Header("Specific UI Components")]
    public Image[] buttonImages; // Button Image components so we can set background image
    public Image backgroundGradientContainer;
    public Image backgroundImageContainer;
    public Button startButton;
    public TextMeshProUGUI startButtonText;
    public TextMeshProUGUI versionText;

    [Header("Buttons")]
    public Button resumeButton;
    public Button optionsButton;
    public Button controlsButton;
    public Button quitButton;

    /*[Header("Background Blur")]
    public bool enableBackgroundBlur;
    public Material blurMaterial;
    public BlurKernelSize kernelSize = BlurKernelSize.Small;
    [Range(0f, 1f)]
	public float interpolation = 1f;
	[Range(0, 4)]
	public int downsample = 1;
	[Range(1, 16)]
	public int iterations = 1;
	public bool gammaCorrection = true;*/

    // Events
    public delegate void OnMusicVolumeChangedDelegate(float newVolume);
    public static OnMusicVolumeChangedDelegate musicVolumeChangedDelegate;

    public delegate void OnEffectsVolumeChangedDelegate(float newVolume);
    public static OnEffectsVolumeChangedDelegate effectsVolumeChangedDelegate;

    private bool defaultCursorVisibility;
    private Level currentLevel;
    private MenuMode currentMode;
    private bool isMenuShowing;
    private bool canListenForInput;
    
#region Init
    // Subscribe to events
    void OnEnable()
    {
        GameManager.levelChangeDelegate += OnLevelChange;

        menuTimeline.fadeInBeginDelegate += OnMenuFadeInBegin;
        menuTimeline.fadeInCompleteDelegate += OnMenuFadeInComplete;
        menuTimeline.fadeOutBeginDelegate += OnMenuFadeOutBegin;
        menuTimeline.fadeOutCompleteDelegate += OnMenuFadeOutComplete;
    }
    void OnDisable()
    {
        GameManager.levelChangeDelegate -= OnLevelChange;
        
        menuTimeline.fadeInBeginDelegate -= OnMenuFadeInBegin;
        menuTimeline.fadeInCompleteDelegate -= OnMenuFadeInComplete;
        menuTimeline.fadeOutBeginDelegate -= OnMenuFadeOutBegin;
        menuTimeline.fadeOutCompleteDelegate -= OnMenuFadeOutComplete;
    }

    void Start()
    {
        defaultCursorVisibility = Cursor.visible;

        // Hide entire menu UI
        menuContainer.gameObject.SetActive(false);

        SetVersionText();
    }
#endregion

    void Update()
    {
        if(isMenuShowing && canListenForInput)
        {
            if(Input.GetKeyDown(KeyCode.R))
                OnResumeBtnPressed();
            else if(Input.GetKeyDown(KeyCode.O))
                OnOptionsBtnPressed();
            else if(Input.GetKeyDown(KeyCode.C))
                OnControlsBtnPressed();
            else if(Input.GetKeyDown(KeyCode.Q))
                OnQuitBtnPressed();
        }
    }

    public void ToggleVisibility(MenuMode menuMode)
    {
        // Only remember menu mode when entering menu
        if(!isMenuShowing)
            currentMode = menuMode;

        menuTimeline.ToggleVisibility();
    }

#region Callback Events
    private void OnMenuFadeInBegin()
    {
        menuContainer.gameObject.SetActive(true);
        isMenuShowing = true;

        //CaptureScreen(); 
        SetStartButtonText();

        // Stop updating level and pause ambient animations
        if(currentLevel != null)
        {
            currentLevel.StopUpdating();
            ambientAnimations.gameObject.SetActive(false);
        }

        Cursor.visible = true;
    }

    private void OnMenuFadeInComplete()
    {
        canListenForInput = true; 

        resumeButton.Select();
    }

    private void OnMenuFadeOutBegin()
    {
        canListenForInput = false;
        
        if(currentMode == MenuMode.StartScreen)
        {
            // When in menu mode, we need to swap the soundtracks when the game starts
            // (In pause mode, level music continues to play during menu screen)
            AudioManager.Instance.StopMusicOfType(MusicType.Menu);
            AudioManager.Instance.PlayMusicOfType(MusicType.Level);
        }

        // Force close all sub-menus
        optionsTimeline.FadeOut();
        controlsTimeline.FadeOut();
    }

    private void OnMenuFadeOutComplete()
    {
        // Start updating level and re-enable ambient animations
        if(currentLevel != null)
        {
            currentLevel.StartUpdating();
            ambientAnimations.gameObject.SetActive(true);
        }  

        Cursor.visible = defaultCursorVisibility;
        isMenuShowing = false;

         // Disable container now UI has completely faded out
        menuContainer.gameObject.SetActive(false);
    }

    private void OnLevelChange(Level newLevel)
    {
        // Keep track of current level so we can enable/disable it
        currentLevel = newLevel;
    }
#endregion

    // NOT USED
    private void CaptureScreen()
    {
        // Render screen into render texture
        RenderTexture renderTexture = new RenderTexture(Screen.width, Screen.height, 24);
        Camera.main.targetTexture = renderTexture;
        Camera.main.Render();

        // Create a new render texture to store the blurred image
        RenderTexture blurredRenderTexture = new RenderTexture(Screen.width, Screen.height, 24);

        // Make blur resolution independent (low intensity on lower resolutions)
        //interpolation = Mathf.Min(1.0f, Screen.height / 2160.0f);

       /* if(enableBackgroundBlur)
        {
            // Blit to another render texture so blurring is done on the GPU (and runs once)
            // Not using a post processing effect as that would run every frame
            // Background is static so blur only needs to run when entering menu screen
            Blur(renderTexture, blurredRenderTexture);

            RenderTexture.active = blurredRenderTexture;
        }
        else*/
        {
            RenderTexture.active = renderTexture;
        } 
       
        // Read active render texture into a Texture2D object
        Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenshot.Apply();

        backgroundImageContainer.sprite = Sprite.Create(screenshot, new Rect(0, 0, Screen.width, Screen.height), Vector2.zero);

        for(int i = 0; i < buttonImages.Length; i++)
        {
            if(buttonImages[i] != null)
            {
                buttonImages[i].sprite = backgroundImageContainer.sprite;
            }
        }

        Camera.main.targetTexture = null;
        RenderTexture.active = null;
        Destroy(renderTexture);
        Destroy(blurredRenderTexture);
    }

    public bool IsVisible()
    {
        return isMenuShowing;
    }

    private void SetStartButtonText()
    {
        // Only use "Start" terminology if game is starting from level 1
        if(currentMode == MenuMode.StartScreen && currentLevel.GetLevelID() == 0)
        {
            startButtonText.text = "Start Game";
        }
        else
        {
            startButtonText.text = "Resume Game";
        }
    }

    private void SetVersionText()
    {
        if(versionText != null)
        {
            // Pull version num from build settings
            versionText.text = "Version " + Application.version;
        }
    }

#region UI Events

    public void OnResumeBtnPressed()
    {
        resumeButton.Select();
        ToggleVisibility(currentMode);
    }

    public void OnOptionsBtnPressed()
    {
        optionsButton.Select();
        optionsTimeline.ToggleVisibility();
        controlsTimeline.FadeOut();
    }

    public void OnControlsBtnPressed()
    {
        controlsButton.Select();
        controlsTimeline.ToggleVisibility();
        optionsTimeline.FadeOut();
    }

    public void OnQuitBtnPressed()
    {
        quitButton.Select();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    public void OnMusicVolumeChanged(float value)
    {
        if(musicVolumeChangedDelegate != null)
        {
            musicVolumeChangedDelegate(value);
        }
    }

    public void OnSoundEffectVolumeChanged(float value)
    {
        if(effectsVolumeChangedDelegate != null)
        {
            effectsVolumeChangedDelegate(value);
        }
    }

#endregion

/*
#region Blur Helpers
    // Extracted from "SuperBlur" project
    // https://github.com/PavelDoGreat/Super-Blur
    private void Blur(RenderTexture source, RenderTexture destination) 
    {
        if (gammaCorrection)
            Shader.EnableKeyword("GAMMA_CORRECTION");
        else
            Shader.DisableKeyword("GAMMA_CORRECTION");

        int kernel = 0;

        switch (kernelSize)
        {
        case BlurKernelSize.Small:
            kernel = 0;
            break;
        case BlurKernelSize.Medium:
            kernel = 2;
            break;
        case BlurKernelSize.Big:
            kernel = 4;
            break;
        }

        var tempRenderTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);

        for (int i = 0; i < iterations; i++)
        {
            // Helps to achieve a larger blur
            float radius = (float)i * interpolation + interpolation;
            blurMaterial.SetFloat("_Radius", radius);

            Graphics.Blit(source, tempRenderTexture, blurMaterial, 1 + kernel);
            source.DiscardContents();

            // If it is the final iteration, blit to destination instead of temp texture
            if (i == iterations - 1)
            {
                Graphics.Blit(tempRenderTexture, destination, blurMaterial, 2 + kernel);
            }
            else
            {
                Graphics.Blit(tempRenderTexture, source, blurMaterial, 2 + kernel);
                tempRenderTexture.DiscardContents();
            }
        }

        RenderTexture.ReleaseTemporary(tempRenderTexture);
		
    }

    public enum BlurKernelSize
    {
        Small,
        Medium,
        Big
    }
#endregion
*/
    
}
