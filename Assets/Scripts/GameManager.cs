﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices; // To call JS functions from C# (set header colour)

// Structure inspired in-part by the incomplete code at:
// https://www.wiktorzychla.com/2011/05/implementing-boulderdashheartlight.html

public class GameManager : MonoBehaviour
{
    public const int TICKS_PER_SECOND = 8;

    [Header("Game Managers")]
    public LevelSpawner levelSpawner;
    public MenuScreenController menuScreenController;
    public AlertManager alertManager;

    [Header("Transitions")]
    public float levelChangeFadeDuration; // Screen fade speed during level transitions
    public float levelLoadSettleTime; // Time after loading a level before it becomes active
    private bool isChangingLevels; // Is level transition currently playing
    private bool isLevelReady; // Level not ready until settle time is complete

    [Header("Controls")]
    public KeyCode pauseKey;
    public KeyCode pauseKeyAlternative;

    // Game data
    private int currentLevelIndex; // ID of current active level
    private Level currentLevel;

    // Delegate events
    public delegate void OnLevelChangeDelegate(Level newLevel);
    public static OnLevelChangeDelegate levelChangeDelegate;

    // Declare external JS functions
    [DllImport("__Internal")]
    private static extern void SetHeaderColour(int headerColour);

#region Init
    void Awake()
    {
        // Don't need PhysX running in the background - using manual physics
        Physics.autoSimulation = false;
        Physics2D.autoSimulation = false;
        Cursor.visible = true;

        // Ensure player doesn't see anything duing initialisation
        CameraFader.Instance.JumpToBlack();

        // Parse levels before initialising memory pools so we can size pools based on demand
        levelSpawner.ReadLevelsFromFile();
        levelSpawner.InitMemoryPools();

        currentLevelIndex = PlayerPrefs.GetInt("currentLevelIndex");
        currentLevel = CreateLevel(currentLevelIndex);

        QualitySettings.vSyncCount = 0;
        if(Application.platform != RuntimePlatform.WebGLPlayer)
        {
            // Simulate start button press if not running on WebGL
            OnStartBtnPressed();
        }
    }

    // Called from JS when "Start" button is clicked
    public void OnStartBtnPressed()
    {
        // Run callback on game load
        if(levelChangeDelegate != null)
        {
            levelChangeDelegate(currentLevel);
        }

        // Set header colour to match level's brick colour (if on WebGL)
        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SetHeaderColour((int)currentLevel.GetBrickColour());
        }

        currentLevel.StartUpdating();
        isLevelReady = true;
        
        CameraFader.Instance.JumpToClear();
        AudioManager.Instance.PlayMusicOfType(MusicType.Level);
    }

    // Called from JS when "Start" button is clicked
    public void InformOfBrowser(string browserName)
    {
        if(browserName == "Safari")
        {
            // Show "click to unmute" alert messge
            // In safari, actual WebGL content (the Unity game) has to be clicked to start audio
            // In other browsers, only the HTML page needs to be clicked (so "start" button on loading screen is enough)
            alertManager.ShowAlert("Click To Unmute", 4.0f);
        }
    }

    void OnApplicationQuit()
    {
       SaveProgress();
    }

    void OnEnable()
    {
        // Subscribe to delegate events
        Level.levelCompleteDelegate += OnLevelComplete;
        Player.playerDiedDelegate += OnPlayerDead;
    }

    void OnDisable()
    {
        Level.levelCompleteDelegate -= OnLevelComplete;
        Player.playerDiedDelegate -= OnPlayerDead;
    }

    void SaveProgress()
    {
        // Remember which level player is up to
        PlayerPrefs.SetInt("currentLevelIndex", currentLevelIndex);
    }
#endregion

    void Update()
    {
        // Level swapping - only allow when menu is not showing
        if(menuScreenController.IsVisible() == false && Input.GetKey(KeyCode.Space))
        {
            if(Input.GetKeyDown(KeyCode.LeftArrow))
            {
                MoveToPreviousLevel();
            }
            else if(Input.GetKeyDown(KeyCode.RightArrow))
            {
                MoveToNextLevel();
            }
        }

        // Show or hide the pause/menu screen
        if(isLevelReady && (Input.GetKeyDown(pauseKey) || Input.GetKeyDown(pauseKeyAlternative)))
        {
            ToggleMenuScreen();
        }

        // TODO handle pause while player is exploding
    }

     // Called from Update() when pause key is pressed
     // Called from JS when HTML "Settings" button is clicked
    public void ToggleMenuScreen()
    {
        if(menuScreenController != null)
        {
            menuScreenController.ToggleVisibility(MenuMode.PauseScreen);
        }
    }

#region Callback Events
    public void OnLevelComplete(int levelID)
    {
        // Delegate event fired by Level when player walks into the door
        MoveToNextLevel();
    }

    public void OnPlayerDead(int levelID)
    {
        // Delegate event fired by Player when it explodes
        StartCoroutine(ResetLevel());
    }
#endregion

#region Level Swapping
    private Level CreateLevel(int levelIndex)
    {
        currentLevelIndex = levelIndex;

        Level newLevel = levelSpawner.GenerateLevel(levelIndex);

        return newLevel;
    }

    private void MoveToNextLevel()
    {
        if(!isChangingLevels)
        {
            StartCoroutine(TransitionToLevel(GetNextLevelIndex(), Vector3.left));
        }
    }

    private void MoveToPreviousLevel()
    {
        if(!isChangingLevels)
        {
            StartCoroutine(TransitionToLevel(GetPreviousLevelIndex(), Vector3.right));
        }
    }

    // Slide current level off the screen, and fade in new level
    IEnumerator TransitionToLevel(int nextLevelIndex, Vector3 transitionDirection)
    {
        isChangingLevels = true;
        isLevelReady = false;
        currentLevel.StopUpdating();

        CameraFader.Instance.FadeToBlack(levelChangeFadeDuration);

        SlideObject levelAnimator = currentLevel.GetComponent<SlideObject>();
        levelAnimator.SlideToPosition(transitionDirection * (Level.LEVEL_WIDTH + 1), 0.3f);
        yield return new WaitForSeconds(levelChangeFadeDuration);

        // Destroy current level now the screen is fully black
        currentLevel.RemoveAllTiles();
        Destroy(currentLevel.gameObject);

        // Create next level while screen is still black
        currentLevel = CreateLevel(nextLevelIndex);

        // Notify subscribers of the level change
        if(levelChangeDelegate != null)
        {
            levelChangeDelegate(currentLevel);
        }

        // Set header colour to match level's brick colour (if on WebGL)
        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SetHeaderColour((int)currentLevel.GetBrickColour());
        }
        
        // Fade in at double speed
        CameraFader.Instance.FadeToClear(levelChangeFadeDuration / 2.0f);
        isChangingLevels = false;

        // Post load settle time before we start updating
        yield return new WaitForSeconds(levelLoadSettleTime);
        currentLevel.StartUpdating();
        isLevelReady = true;

        SaveProgress();
    }

    private int GetNextLevelIndex()
    {
        if(currentLevelIndex + 1 >= levelSpawner.GetLevelCount())
        {
            return 0;
        }
        return currentLevelIndex + 1;
    }

    private int GetPreviousLevelIndex()
    {
        if(currentLevelIndex - 1 < 0)
        {
            return levelSpawner.GetLevelCount() - 1;
        }
        return currentLevelIndex - 1;
    }   
    
    IEnumerator ResetLevel()
    {
        // Wait for level to settle (all objects finished moving/exploding)
        // Simple timer for now so fade-out isn't instant
        yield return new WaitForSeconds(1.0f);

        isChangingLevels = true;
        isLevelReady = false;
        currentLevel.StopUpdating();

        if(menuScreenController.IsVisible() == false)
        {
            CameraFader.Instance.FadeToBlack(levelChangeFadeDuration);
            yield return new WaitForSeconds(levelChangeFadeDuration);
        }
       
        // Re-spawn level now screen is fully black
        currentLevel.RemoveAllTiles();
        Destroy(currentLevel.gameObject);

        currentLevel = CreateLevel(currentLevelIndex);

        // Notify subscribers of the level change
        if(levelChangeDelegate != null)
        {
            levelChangeDelegate(currentLevel);
        }

        if(menuScreenController.IsVisible() == false)
        {
            CameraFader.Instance.FadeToClear(levelChangeFadeDuration);
        }
        
        isChangingLevels = false;

        yield return new WaitForSeconds(levelLoadSettleTime);
        currentLevel.StartUpdating();
        isLevelReady = true;
    }

#endregion

}
