﻿using UnityEngine;

public static class MathsHelper {

	public static float RemapRange(float value, float fromMin, float fromMax, float toMin, float toMax)
	{
		return toMin + (value - fromMin) * (toMax - toMin) / (fromMax - fromMin);
	}
	
    public static bool CompareFloat(float value, float target, float tolerance)
    {
        return Mathf.Abs(value - target) <= tolerance;
    }

}
