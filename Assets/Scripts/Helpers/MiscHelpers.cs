﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiscHelpers : MonoBehaviour
{
    public static Vector2Int GetOffsetForDirection(Direction direction)
    {
        // 0,0 (x,y) is bottom left
        switch(direction)
        {
            case Direction.N:
                return new Vector2Int(0,1);
            case Direction.NE:
                return new Vector2Int(1, 1);
            case Direction.E:
                return new Vector2Int(1, 0);
            case Direction.SE:
                return new Vector2Int(1, -1);
            case Direction.S:
                return new Vector2Int(0, -1);
            case Direction.SW:
                return new Vector2Int(-1, -1);
            case Direction.W:
                return new Vector2Int(-1, 0);
            case Direction.NW:
                return new Vector2Int(-1, 1);
            default:
                return Vector2Int.zero;
        }
    }
}
