﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AlertManager : MonoBehaviour
{
    public UIAnimation alertAnimation;
    public TextMeshProUGUI alertText;

    private float timeout; // Only handles 1 visible alert at a time

    void OnEnable()
    {
        alertAnimation.fadeInCompleteDelegate += OnAlertFadeInComplete;
    }
    void OnDisable()
    {
        alertAnimation.fadeInCompleteDelegate -= OnAlertFadeInComplete;
    }

    public void ShowAlert(string message, float timeout)
    {
        alertAnimation.FadeIn();
        this.timeout = timeout;

        if(alertText != null)
        {
            alertText.text = message;
        }
    }

    public void HideAlert()
    {
        // Start fade out timeline
        alertAnimation.FadeOut();
    }

#region Events
    public void OnAlertClicked()
    {
        HideAlert();
    }

    public void OnAlertFadeInComplete()
    {
        Invoke("HideAlert", timeout);
    }

#endregion
}
