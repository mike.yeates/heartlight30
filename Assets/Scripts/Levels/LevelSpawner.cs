﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : MonoBehaviour
{
    [SerializeField] private LevelParser levelParser;
    [SerializeField] private Transform levelParent;
    [SerializeField] private Level levelTemplate;

    [Header("Memory Pools")]
    public ExplosionPool explosionPool;
    public GrassPool grassPool;
    public HeartPool heartPool;
    public RockPool rockPool;
    public BombPool bombPool;
    public BalloonPool balloonPool;
    public BlankPool emptyPool;
    public MetalWallPool metalWallPool;
    public BrickWallPool brickWallPool;
    public PlasmaPool plasmaPool;
    public WarpLeftPool warpLeftPool;
    public WarpRightPool warpRightPool;
    public DoorPool doorPool;
    public PercivalPool percivalPool;

    [Header("Sprite Variations")]
    [SerializeField] private Sprite[] grassVariations;
    [SerializeField] private Sprite brownBrickWall;
    [SerializeField] private Sprite burgundyBrickWall;
    [SerializeField] private Sprite purpleBrickWall;
    [SerializeField] private Sprite violetBrickWall;

    private List<LevelBlueprint> levels; // The master list of unmodified levels

    public int ReadLevelsFromFile()
    {
        levels = levelParser.ParseLevels();
        return levels.Count;
    }

    public void InitMemoryPools()
    {
        // Set size of each memory pool to be the most required by any one level
        grassPool.Init(levelParser.GetMaxUseCountForTileType(TileType.grass));
        heartPool.Init(levelParser.GetMaxUseCountForTileType(TileType.heart));
        rockPool.Init(levelParser.GetMaxUseCountForTileType(TileType.rock));
        bombPool.Init(levelParser.GetMaxUseCountForTileType(TileType.bomb));
        balloonPool.Init(levelParser.GetMaxUseCountForTileType(TileType.balloon));
        metalWallPool.Init(levelParser.GetMaxUseCountForTileType(TileType.metalWall));
        brickWallPool.Init(levelParser.GetMaxUseCountForTileType(TileType.brickWall));
        plasmaPool.Init(levelParser.GetMaxUseCountForTileType(TileType.plasma));
        warpLeftPool.Init(levelParser.GetMaxUseCountForTileType(TileType.tunnelLeft));
        warpRightPool.Init(levelParser.GetMaxUseCountForTileType(TileType.tunnelRight));
        doorPool.Init(levelParser.GetMaxUseCountForTileType(TileType.door));
        percivalPool.Init(levelParser.GetMaxUseCountForTileType(TileType.dwarf));

        // Explosion and empty tiles can spawn after level creation
        explosionPool.Init(Level.LEVEL_WIDTH * Level.LEVEL_HEIGHT);
        emptyPool.Init(Level.LEVEL_WIDTH * Level.LEVEL_HEIGHT); 
    }

    public Level GenerateLevel(int levelIndex)
    {
        LevelBlueprint blueprint = levels[levelIndex];

        // Spawn new level in the world ready to populate with tiles
        Level level = Instantiate(levelTemplate, Vector3.zero, Quaternion.identity, levelParent) as Level;
        level.gameObject.name = "Level " + levelIndex;
        level.Init(blueprint, this);

        for(int y = 0; y < Level.LEVEL_HEIGHT; y++)
        {
            for(int x = 0; x < Level.LEVEL_WIDTH; x++)
            {
                BaseTile tile = GetObjectForTile(blueprint.data[y,x], levelIndex);

                if(tile != null)
                {
                    if(!level.AddTileAtPosition(tile, new Vector2Int(x, y)))
                    {
                        Debug.Log("Level " + levelIndex + " rejected tile of type " + tile.ToString() + " at position " + x + "," + y);
                    }
                }
            }
        }

        return level;
    }

    private BaseTile GetObjectForTile(TileType tile, int levelID)
    {
        switch(tile)
        {
            case TileType.empty:
                return emptyPool.Get();
            case TileType.heart:
                return heartPool.Get();
            case TileType.rock:
                return rockPool.Get();
            case TileType.grass:
                return GetGrassTile();
            case TileType.dwarf:
                return percivalPool.Get();
            case TileType.door:
                return doorPool.Get();
            case TileType.bomb:
                return bombPool.Get();
            case TileType.balloon:
                return balloonPool.Get();
            case TileType.plasma:
                return plasmaPool.Get();
            case TileType.brickWall:
                return GetBrickWall(levelID);
            case TileType.metalWall:
                return metalWallPool.Get();
            case TileType.tunnelLeft:
                return warpLeftPool.Get();
            case TileType.tunnelRight:
                return warpRightPool.Get();
            default:
                return emptyPool.Get();   
        }
    }

    private BaseTile GetBrickWall(int levelID)
    {
        BrickWall wall = brickWallPool.Get();
        SpriteRenderer wallRenderer = wall.GetComponent<SpriteRenderer>();

        // Set texture of brick wall to match colour requested by level
        switch(levels[levelID].brickColour)
        {
            case BrickColour.brown:
                wallRenderer.sprite = brownBrickWall;
                break;
            case BrickColour.burgundy:
                wallRenderer.sprite = burgundyBrickWall;
                break;
            case BrickColour.purple:
                wallRenderer.sprite = purpleBrickWall;
                break;
            case BrickColour.violet:
                wallRenderer.sprite = violetBrickWall;
                break;
            default:
                wallRenderer.sprite = burgundyBrickWall;
                break;
        }

        return wall;
    }

    private BaseTile GetGrassTile()
    {
        Grass grass = grassPool.Get();

        // Pick random grass texture
        grass.GetComponent<SpriteRenderer>().sprite = grassVariations[Random.Range(0, grassVariations.Length)];

        return grass;
    }

    public int GetLevelCount()
    {
        return levels.Count;
    }

    // Called by Level when it wants to remove a tile
    public void ReturnTileToPool(BaseTile tile)
    {
        // ObjectPool will disable the tile are re-parent it back to its memory pool container

        if(tile is Explosion)
            explosionPool.ReturnObject(tile as Explosion);
        else if(tile is Grass)
            grassPool.ReturnObject(tile as Grass);
        else if(tile is Heart)
            heartPool.ReturnObject(tile as Heart);
        else if(tile is Rock)
            rockPool.ReturnObject(tile as Rock);
        else if(tile is Bomb)
            bombPool.ReturnObject(tile as Bomb);
        else if(tile is Balloon)
            balloonPool.ReturnObject(tile as Balloon);
        else if(tile is Empty)
            emptyPool.ReturnObject(tile as Empty);
        else if(tile is MetalWall)
            metalWallPool.ReturnObject(tile as MetalWall);
        else if(tile is BrickWall)
            brickWallPool.ReturnObject(tile as BrickWall);
        else if(tile is Plasma)
            plasmaPool.ReturnObject(tile as Plasma);
        else if(tile is Warp)
        {
            Warp warpTile = tile as Warp;
            if(warpTile.warpDirection == Direction.E)
                warpRightPool.ReturnObject(warpTile);
            else if(warpTile.warpDirection == Direction.W)
                warpLeftPool.ReturnObject(warpTile);
        }
        else if(tile is Door)
            doorPool.ReturnObject(tile as Door);
        else if(tile is Player)
            percivalPool.ReturnObject(tile as Player);
    }

}
