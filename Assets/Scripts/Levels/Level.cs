﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction { None, N, NE, E, SE, S, SW, W, NW }

public class Level : MonoBehaviour
{
    public const int LEVEL_WIDTH = 20;
    public const int LEVEL_HEIGHT = 12;

    private LevelBlueprint levelBlueprint; // A copy of the blueprint we are based on (houses author, id, and other data)

    private int numHeartsToCollect; // Total hearts placed in level
    private int numHeartsCollected; // Hearts player has collected for this level

    private LevelSpawner levelSpawner; // So we can return tiles to the memory pool

    // 0,0 is bottom left. Array indices matches world-space pos.
    private BaseTile[,] tiles = new BaseTile[LEVEL_HEIGHT, LEVEL_WIDTH];

    // Tiles this level manages which aren't currently visible (i.e Player inside a warp strip)
    // Still need to reference these objects so we can return them to the memory pool if level change
    // occurs while player is inside warp strip.
    private List<BaseTile> hiddenTiles;

    private bool allowedToStepPhysics;
    private float timeUntilNextPhysicsStep;

#region Events
    // Event triggered when player enters door
    public delegate void OnEnterDoorDelegate();
    public static OnEnterDoorDelegate enterDoorDelegate;

    // Event triggered when player celebration animation completes
    public delegate void OnLevelCompleteDelegate(int levelID);
    public static OnLevelCompleteDelegate levelCompleteDelegate;

    // Event triggered when player collects a heart
    public delegate void OnHeartCollectedDelegate(int heartsCollected);
    public static OnHeartCollectedDelegate heartCollectedDelegate;
#endregion

    void Start()
    {
        numHeartsCollected = 0;
        timeUntilNextPhysicsStep = 1.0f / GameManager.TICKS_PER_SECOND;
        hiddenTiles = new List<BaseTile>();
    }

    public void Init(LevelBlueprint blueprint, LevelSpawner tileSpawner)
    {
        this.levelBlueprint = blueprint;
        this.levelSpawner = tileSpawner;
    }

    public void StartUpdating()
    {
        allowedToStepPhysics = true;
        Player.ListenForInput();

        // Some levels have no hearts, so open door(s) from the start
        if(IsLevelComplete())
            OpenDoors();
    }

    public void StopUpdating()
    {
        allowedToStepPhysics = false;
        Player.StopListeningForInput();
    }

    void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.Space))
        {
            StepPhysics();
        }*/

        if(allowedToStepPhysics)
        {
            timeUntilNextPhysicsStep -= Time.deltaTime;

            if(timeUntilNextPhysicsStep <= 0.0f)
            {
                timeUntilNextPhysicsStep += (1.0f / GameManager.TICKS_PER_SECOND);
                StepPhysics();
            }
        }
    }

    private void StepPhysics()
    {
        // Pre-update
        // 0,0 (x,y) is bottom left. Step top-down.
        for(int y = LEVEL_HEIGHT - 1; y >= 0; y--) 
        {
            for(int x = 0; x < LEVEL_WIDTH; x++)
            {   
                if(tiles[y,x] != null)
                {
                    tiles[y,x].PreUpdate();
                }
            }
        }

        for(int y = LEVEL_HEIGHT - 1; y >= 0; y--)
        {
            for(int x = 0; x < LEVEL_WIDTH; x++)
            {   
                if(tiles[y,x] != null)
                {
                    tiles[y,x].StepPhysics();
                }
            }
        }
    }  

#region Movement
    public bool MovePlayer(Player player, Vector2Int targetPosition, Direction direction)
    {
        if(!WithinLevelBounds(targetPosition))
            return false;
            
        // Early movement validation (later rules can over-rule this)
        if(!CanPlayerMoveHere(targetPosition))
            return false;

        // Push logic
        if(tiles[targetPosition.y, targetPosition.x].CanBePushed)
        {   
            // TODO detect when push starts so slow-down is instant and not after first push
            if(!player.AllowedToPush())
                return false;

            if(!PushTile(player, targetPosition, direction))
                return false;
        }

        // Warp logic
        if(tiles[targetPosition.y, targetPosition.x] is Warp)
        {
            WarpSequence(player, targetPosition, direction);

            // If warp succeeds, return false so MoveTile doesn't run (warp is now in charge of moving player)
            // If warp is not successfull, also return false so player doesn't move into strip
            return false;
        }

        // Door logic
        if(tiles[targetPosition.y, targetPosition.x] is Door)
        {
            StartCoroutine(CompleteLevelSequence());
        }

        // Heart logic
        CheckForAndEatHeart(targetPosition);

        return MoveTile(player, targetPosition);
    }

    public bool MoveTile(BaseTile tile, Vector2Int targetPosition)
    {
        if(!WithinLevelBounds(targetPosition))
            return false;

        // Remove tile from current position
        tiles[tile.Position.y, tile.Position.x] = null;

        // Spawn an empty tile (grab one from the memory pool)
        AddEmptyTileAtPosition(tile.Position);

        // Destroy tile at destination
        // Likely an empty tile, grass, or heart
        RemoveTile(GetTileAtPosition(targetPosition));

        // Update 2D array pointers so tile is now at correct index given targetPosition
        tiles[targetPosition.y, targetPosition.x] = tile;

        // If this function succeeds, BaseTile sets its own Position property, which also updates transfrom

        return true;
    }

    private bool CanPlayerMoveHere(Vector2Int pos)
    {
        if(tiles[pos.y, pos.x].CanBeEaten)
            return true;
        
        if(tiles[pos.y, pos.x].CanBePushed)
            return true;

        if(tiles[pos.y, pos.x] is Empty)
            return true;

        if(tiles[pos.y, pos.x] is Door)
        {
            Door door = tiles[pos.y, pos.x] as Door;
            if(door.IsOpen())
            {
                return true;
            }
        }

        if(tiles[pos.y, pos.x] is Warp)
            return true;
        
        return false;
    }
#endregion

#region Push
    private bool PushTile(Player player, Vector2Int positionToPush, Direction direction)
    {
        // Can only push left or right
        if(direction != Direction.E && direction != Direction.W)
            return false;

        // Don't push object if it is about to fall
        if(tiles[positionToPush.y, positionToPush.x].ReceivesGravity &&
            tiles[positionToPush.y, positionToPush.x].GetNeighbour(Direction.S) is Empty)
          return false;

        // Cannot push ojects already falling 
        if(tiles[positionToPush.y, positionToPush.x].GetCurrentStepMoveDirection() == Direction.S)
          return false;

        // Push destination is the space we want to push a tile into
        Vector2Int pushDestination = positionToPush + MiscHelpers.GetOffsetForDirection(direction);
        
        if(!WithinLevelBounds(pushDestination))
            return false;
        
        // The tile at this space
        BaseTile pushDestinationTile = tiles[pushDestination.y, pushDestination.x];

        // Is there is a free space to push this object into
        if(pushDestinationTile != null && pushDestinationTile is Empty)
        {
            // Push object
            tiles[positionToPush.y, positionToPush.x].MoveTo(direction);
            return true;
        }
        else
        {
            // No room to push object
            return false;
        }
    }
#endregion

#region Warp
    private void WarpSequence(Player player, Vector2Int originPos, Direction direction)
    {
        Warp originTile = tiles[originPos.y, originPos.x] as Warp;

        // Are we trying to enter warp strip from the correct side
        if(direction == originTile.warpDirection)
        {
            List<BaseTile> warpStrip = GetWarpStrip(originTile, direction);

            // Is there room on the other side to exit the warp strip
            BaseTile departureTile = warpStrip[warpStrip.Count - 1];

            if(departureTile is Empty ||
               departureTile is Grass)
               // Can't warp into heart in original game
            {
                AudioManager.Instance.PlaySoundAtPosition(SoundType.Warp, originPos);
                StartCoroutine(WarpStripAnimationSequence(warpStrip, player));
            }
        }  
    }

    IEnumerator WarpStripAnimationSequence(List<BaseTile> warpStrip, Player player)
    {
        // Animates warp strips and teleports player to other side

         // Remove reference to player from current position
        tiles[player.Position.y, player.Position.x] = null;

        // Add player reference to hidden tiles list
        hiddenTiles.Add(player);
        
        // Add an empty tile where player is standing
        AddEmptyTileAtPosition(player.Position);

        // Stop player listening to input and hide
        player.gameObject.SetActive(false);

        for(int i = 0; i < warpStrip.Count; i++)
        {
            // Step through warp tiles (final tile is departure tile)
            if(i < warpStrip.Count - 1)
            {
                Warp warpTile = warpStrip[i] as Warp;
                warpTile.Squash();
                yield return new WaitForSeconds(1.0f / GameManager.TICKS_PER_SECOND);
            }
            else
            {
                // Reached the end of the strip (the departure tile)
                BaseTile departureTile = warpStrip[i];
                Vector2Int departurePos = departureTile.Position;

                // Destroy tile at departurePos
                RemoveTile(departureTile);

                // Update 2D array references so player is now indexed at departurePos
                tiles[departurePos.y, departurePos.x] = player;

                // Remove player fro hidden tiles list
                hiddenTiles.Remove(player);
                
                player.gameObject.SetActive(true);

                // Inform player of its new position so its gameobject renders at correct location
                player.Position = departurePos;
            }
        }
    }

    private List<BaseTile> GetWarpStrip(Warp originTile, Direction direction)
    {
        // Gather all consecutive warp tiles that make up a connected strip
        List<BaseTile> warpStrip = new List<BaseTile>();

        bool foundEndOfStrip = false;
        Vector2Int nextPosToCheck = originTile.Position;
        while(!foundEndOfStrip && WithinLevelBounds(nextPosToCheck))
        {
            if(tiles[nextPosToCheck.y, nextPosToCheck.x] is Warp)
            {
                warpStrip.Add(tiles[nextPosToCheck.y, nextPosToCheck.x]);
                nextPosToCheck += MiscHelpers.GetOffsetForDirection(direction);
            }
            else
            {
                // Also add departure tile (not of type Warp) for convenience
                warpStrip.Add(tiles[nextPosToCheck.y, nextPosToCheck.x]);
                foundEndOfStrip = true;
            }
        }

        return warpStrip;
    }
#endregion

#region Explosions
    public bool TriggerExplosionFromPosition(Vector2Int pos)
    {
        if(!WithinLevelBounds(pos))
            return false;

        BaseTile explosionOrigin = GetTileAtPosition(pos);

        // Before we replace tile with an explosion, remember whether it was a bomb
        // (controls whether explosion spreads to neighbours)
        bool didTriggerSequence = explosionOrigin.TriggersExplosion;
        
        Explosion explosionTile = AddExplosionTileAtPosition(pos);
        explosionTile.StartExplosion(didTriggerSequence);

        return true;
    }
#endregion

#region Spawning / Despawning
    public Explosion AddExplosionTileAtPosition(Vector2Int pos)
    {
        RemoveTile(GetTileAtPosition(pos));
        Explosion explosionTile = levelSpawner.explosionPool.Get();

        if(AddTileAtPosition(explosionTile, pos))
        {
            return explosionTile;
        }
        return null;
    }

    public Empty AddEmptyTileAtPosition(Vector2Int pos)
    {
        RemoveTile(GetTileAtPosition(pos));
        Empty emptyTile = levelSpawner.emptyPool.Get();

        if(AddTileAtPosition(emptyTile, pos))
        {
            return emptyTile;
        }
        return null;
    }

    public bool AddTileAtPosition(BaseTile tile, Vector2Int pos)
    {
        if(!WithinLevelBounds(pos))
            return false;

        tile.gameObject.SetActive(true); // Tile are not active when in the pool

        // Inform new tile of the level it belongs to and also its position
        // Also resets state of tile as it is recycled from memory pool
        tile.Init(this, new Vector2Int(pos.x, pos.y));

        // Re-parent tile to this level
        tile.transform.parent = transform;

        // Update tiles array so it references new tile at correct index
        tiles[tile.Position.y, tile.Position.x] = tile;        

        if(tile is Heart)
        {
            numHeartsToCollect++;
        }

        return true;
    }

    public void RemoveTile(BaseTile tileToRemove)
    {
        // Add back to the memory pool
        if(tileToRemove != null)
            levelSpawner.ReturnTileToPool(tileToRemove);
    }

    public void RemoveAllTiles()
    {
        for(int y = LEVEL_HEIGHT - 1; y >= 0; y--) 
        {
            for(int x = 0; x < LEVEL_WIDTH; x++)
            {   
                RemoveTile(GetTileAtPosition(new Vector2Int(x, y)));
            }
        }

        // Handle player separately as it is removed from tiles array while in warp strip
        for(int i = 0; i < hiddenTiles.Count; i++)
        {
            RemoveTile(hiddenTiles[i]);
        }
    
    }
#endregion

#region Win Condition
    private bool IsLevelComplete()
    {
        // Called each time a heart is collected
        return numHeartsCollected >= numHeartsToCollect;
    }

    private IEnumerator CompleteLevelSequence()
    {
        // Called when player walks into the open door
        AudioManager.Instance.PlaySound(SoundType.Applause);

        if(enterDoorDelegate != null)
            enterDoorDelegate();

        // Player is subscribed to above event and will start jumping animation

        // Give player time to celebrate (jumping animation)
        yield return new WaitForSeconds(1.25f);

        // Notify subscribers that level is now complete
        if(levelCompleteDelegate != null)
            levelCompleteDelegate(levelBlueprint.id);

        // GameManager is subscribed to above event and will swap to the next level
    }

    private void OpenDoors()
    {
        // Could be multiple doors in level
        for(int i = LEVEL_HEIGHT-1; i >= 0; i--)
        {
            for(int j = 0; j < LEVEL_WIDTH; j++)
            {   
                if(tiles[i,j] != null && tiles[i,j] is Door)
                {
                    Door door = tiles[i,j] as Door;
                    door.OpenDoor();
                }
            }
        }
    }
#endregion

#region Helpers
    public BaseTile GetTileAtPosition(Vector2Int pos)
    {
        if(WithinLevelBounds(pos))
        {
            return tiles[pos.y, pos.x];
        }
        
        return null;
    }

    private void CheckForAndEatHeart(Vector2Int pos)
    {
        if(tiles[pos.y, pos.x] is Heart)
        {
            AudioManager.Instance.PlaySoundAtPosition(SoundType.Collect, pos);
            numHeartsCollected++;

            if(heartCollectedDelegate != null)
            {
                heartCollectedDelegate(numHeartsCollected);
            }

            if(IsLevelComplete())
            {
                OpenDoors();
            }
        }
    }

    public bool WithinLevelBounds(Vector2Int pos)
    {
        if(pos.x < 0 || pos.x >= LEVEL_WIDTH ||
           pos.y < 0 || pos.y >= LEVEL_HEIGHT)
        {
            return false;
        }  
        return true;
    }

    public int GetTotalHearts()
    {
        return numHeartsToCollect;
    }

    public int GetLevelID()
    {
        return levelBlueprint.id;
    }

    public string GetLevelAuthor()
    {
        return levelBlueprint.author;
    }

    public BrickColour GetBrickColour()
    {
        return levelBlueprint.brickColour;
    }

#endregion

}
