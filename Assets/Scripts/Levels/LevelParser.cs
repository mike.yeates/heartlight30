﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

/*
Levels are wrapped inside curly braces and use the following characters:
    empty space          (space)
$   heart                (dollar sign)
@   rock                 (at, SHIFT + 2)
.   grass                (period)
*   the dwarf            (asterisk, SHIFT + 8)
!   exit door            (exclamation point, SHIFT + 1)
&   bomb                 (ampersand, SHIFT + 7)
0   balloon              (the number zero)
=   plasma               (equal sign)
#   brick wall           (number sign, SHIFT + 3)
%   metal wall           (percent, SHIFT + 5)
<   tunnel to the left   (less than sign)
>   tunnel to the right  (greater than sign)
*/

public enum TileType
{
    empty,
    heart,
    rock,
    grass,
    dwarf,
    door,
    bomb,
    balloon,
    plasma,
    brickWall,
    metalWall,
    tunnelLeft,
    tunnelRight,
    explosion,
    count
};

public enum BrickColour
{
    brown,
    burgundy,
    purple,
    violet,
    none,
    count
};

public struct LevelBlueprint
{
    public int id;
    public string author;
    public BrickColour brickColour;
    public TileType[,] data;
};

public class LevelParser : MonoBehaviour
{
    public TextAsset levelsTextFile;
    private int levelCount;

    // Count how many times each tile is used per level, and store the max here to size the memory pools
    private Dictionary<TileType, int> maxTileUseCount;
    private Dictionary<TileType, int> currentTileUseCount; // Count for current level being parsed

    // Delegate events
    public delegate void OnParsingCompleteDelegate(int numLevelsParsed);
    public static OnParsingCompleteDelegate parsingCompleteDelegate;

    void Start()
    {
        levelCount = 0;
    }

    public List<LevelBlueprint> ParseLevels()
    {
        List<LevelBlueprint> levels = new List<LevelBlueprint>();

        currentTileUseCount = new Dictionary<TileType, int>();
        maxTileUseCount = new Dictionary<TileType, int>();

        ClearTileUseCount(maxTileUseCount);

        // Each index of array now contains one line of text
        string[] rawLines = ReadLines();

        string latestAuthor = "";
        BrickColour latestBrickColour = BrickColour.burgundy;

        for(int line = 0; line < rawLines.Length; line++)
        {
            if(rawLines[line].StartsWith("author"))
            {
                // Split line based on ':'
                string[] splitAuthor = rawLines[line].Split(':');
                latestAuthor = splitAuthor[1].Trim();
            }
            else if(rawLines[line].StartsWith("brickColour"))
            {
                // Split line based on ':'
                string[] splitBrickClolour = rawLines[line].Split(':');
                int brickColourIndex = int.Parse(splitBrickClolour[1]);
                if(brickColourIndex < (int)BrickColour.count)
                {
                    latestBrickColour = (BrickColour)brickColourIndex;
                }
            }
            // Levels are wrapped in curly braces
            if(rawLines[line].StartsWith("{"))
            {
                LevelBlueprint level = new LevelBlueprint();
                level.author = latestAuthor;
                level.brickColour = latestBrickColour;

                if(ExtractLevel(rawLines, line, ref level))
                {
                    // Level struct is now populated with level data

                    if(IsValidLevel(level.data))
                    {
                        // Check if tile counts for each type are above the current highest
                        for(int i = 0; i < (int)TileType.count; i ++)
                        {
                            if(currentTileUseCount[(TileType)i] > maxTileUseCount[(TileType)i])
                            {
                                maxTileUseCount[(TileType)i] = currentTileUseCount[(TileType)i];
                            }
                        }

                        // Now level is extratced, jump loop over this level
                        line += Level.LEVEL_HEIGHT;

                        levelCount++;
                        levels.Add(level);
                    }
                }
            }
        }

        if(parsingCompleteDelegate != null)
        {
            parsingCompleteDelegate(levelCount);
        }
    
        return levels;
    }

    private bool ExtractLevel(string[] rawLines, int startLine, ref LevelBlueprint level)
    {
        ClearTileUseCount(currentTileUseCount);

        // Check that level consists of 14 lines
        if(startLine + (Level.LEVEL_HEIGHT + 1) < rawLines.Length)
        {
            // Check if level ends with a closing curly brace
            if(rawLines[startLine + (Level.LEVEL_HEIGHT + 1)].StartsWith("}"))
            {
                // Found a level
                level.id = levelCount;
                level.data = new TileType[Level.LEVEL_HEIGHT, Level.LEVEL_WIDTH];

                for(int line = 0; line < Level.LEVEL_HEIGHT; line++)
                {
                    // Invert height so 0,0 is bottom left of level, not top left
                    string currentLineData = rawLines[startLine + (Level.LEVEL_HEIGHT - line)];

                    // Step through characters for this line, ignoring the newline character at the end
                    for(int column = 0; column < currentLineData.Length - 1; column++)
                    {
                        // Shorthand for charAt (a string acts like a char array)
                        TileType currentTileType = GetTileForChar(currentLineData[column]);
                        level.data[line,column] = currentTileType;
                        currentTileUseCount[currentTileType]++;
                    }
                }
                return true;
            }
        }
        // Level is invalid if it does not contain a closing curly brace 13 lines after the opening
        return false;
    }

    private string[] ReadLines()
    {
        // Returns array where each index contains one line of text file
        return Regex.Split(levelsTextFile.text, "\n");
    }
    
    private bool IsValidLevel(TileType[,] levelData)
    {
        // Includes one dwarf and at least one exit door
        bool hasDwarf = false;
        bool hasDoor = false;

        for(int i = 0; i < Level.LEVEL_HEIGHT; i++)
        {
            for(int j = 0; j < Level.LEVEL_WIDTH; j++)
            {
                if(levelData[i,j] == TileType.dwarf)
                {
                    hasDwarf = true;
                }
                if(levelData[i,j] == TileType.door)
                {
                    hasDoor = true;
                }

                if(hasDoor && hasDwarf)
                    return true;
            }
        }
        
        // Didn't find a door or dwarf if we get out here
        return false;
    }

    private TileType GetTileForChar(char c)
    {
        switch(c)
        {
            case ' ':
                return TileType.empty;
            case '$':
                return TileType.heart;
            case '@':
                return TileType.rock;
            case '.':
                return TileType.grass;
            case '*':
                return TileType.dwarf;
            case '!':
                return TileType.door;
            case '&':
                return TileType.bomb;
            case '0':
                return TileType.balloon;
            case '=':
                return TileType.plasma;
            case '#':
                return TileType.brickWall;
            case '%':
                return TileType.metalWall;
            case '<':
                return TileType.tunnelLeft;
            case '>':
                return TileType.tunnelRight;
            default:
                return TileType.empty;  
        }
    }

    private void ClearTileUseCount(Dictionary<TileType, int> tileUseCount)
    {
        for(int i = 0; i < (int)TileType.count; i ++)
        {
            tileUseCount[(TileType)i] = 0;
        }
    }

    public int GetMaxUseCountForTileType(TileType tileType)
    {
        return maxTileUseCount[tileType];
    }

}
