﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : BaseTile
{
    public override bool CanBePushed
    {
        get
        {
            return true;
        }
    }

    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }

    public override bool IsStableSurface
    {
        get
        {
            return true;
        }
    }

    public override bool ReceivesAntiGravity
    {
        get
        {
            return true;
        }
    }

}
