﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Empty : BaseTile
{
    public override bool CanBeEaten
    {
        get
        {
            return true;
        }
    }

    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }
}
