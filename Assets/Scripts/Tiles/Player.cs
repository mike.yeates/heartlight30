﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BaseTile
{
    private static bool allowedToListenForInput;
    private bool allowedToExplode;

    private float timeUntilMovementAllowed;
    private float timeUntilPushAllowed;

    private const float TIME_BETWEEN_MOVEMENT = 1.0f / GameManager.TICKS_PER_SECOND;
    private const float TIME_BETWEEN_PUSHES = TIME_BETWEEN_MOVEMENT * 2.0f;

    // Event triggered when player explodes
    public delegate void OnPlayerDieDelegate(int levelID);
    public static OnPlayerDieDelegate playerDiedDelegate;

    // While player is moved at 8FPS in sync with physics, input is read every frame
    // (except for a small cooldown after a physics step) and latest input is used
    // when the next physics step occurs.
    private Direction latestInputDirection;

    private PercivalSpritesheetAnimation animationController;


#region Properties
    public override bool IsStableSurface
    {
        get
        {
            return true;
        }
    }
    
    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }
#endregion

#region Init
    void Awake()
    {
        animationController = GetComponent<PercivalSpritesheetAnimation>();
    }

    public override void Init(Level level, Vector2Int position)
    {
        base.Init(level, position);

        // Player's must be told to listen for input by their Level
        allowedToListenForInput = false;
        allowedToExplode = true;

        animationController.Init();
    }

    // Subscribe to events
    void OnEnable()
    {
        Level.enterDoorDelegate += OnPlayerEnteredDoor;
    }
    void OnDisable()
    {
        Level.enterDoorDelegate -= OnPlayerEnteredDoor;
    }
#endregion

    void Update()
    {
        if(timeUntilPushAllowed > 0.0f)
        {
            timeUntilPushAllowed -= Time.deltaTime;
        }

        if(timeUntilMovementAllowed > 0.0f)
        {
            timeUntilMovementAllowed -= Time.deltaTime;
        }

        if(allowedToListenForInput)
        {
            if(Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Delete))
            {
                StartCoroutine(SelfDestructSequence());
            }

            if(timeUntilMovementAllowed <= 0.0f)
            {
                if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                {
                    latestInputDirection = Direction.E;
                }
                else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                {
                    latestInputDirection = Direction.W;
                }
                else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                {
                    latestInputDirection = Direction.S;
                }
                else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                {
                    latestInputDirection = Direction.N;
                }
            }
        }
    }

    public override void StepPhysics()
    {
        // Prevent multiple movements in a single step
        if(currentStepMovementDirection != Direction.None)
            return;

        if(latestInputDirection != Direction.None)
        {
            // Use last received input to move player (input occured between physics steps)
            MovePlayer(latestInputDirection);
        }

        CheckForCrushedPlayer(); // Did anything hard just land on us

        // Input cooldown for (TIME_BETWEEN_MOVEMENT / 2) seconds after each physics step
        timeUntilMovementAllowed = TIME_BETWEEN_MOVEMENT / 2.0f;

        animationController.StepAnimation(latestInputDirection, Position);
        latestInputDirection = Direction.None;
        previousNeighbours = currentNeighbours;
    }

    private void MovePlayer(Direction direction)
    {
        // Ask level to move player
        Vector2Int targetPosition = Position + MiscHelpers.GetOffsetForDirection(direction);
        if(level.MovePlayer(this, targetPosition, direction))
        {
            currentStepMovementDirection = direction;
            Position = targetPosition;
        }
    }

    public bool AllowedToPush()
    {
        // Cooldown between pushes (to slow player down when pushing objects)
        if(timeUntilPushAllowed <= 0.0f)
        {
            timeUntilPushAllowed = TIME_BETWEEN_PUSHES;
            return true;
        }

        return false;
    }

    public static void ListenForInput()
    {
        // Called by Level once fully loaded and settled
        allowedToListenForInput = true;
    }

    public static void StopListeningForInput()
    {
        allowedToListenForInput = false;
    }

    IEnumerator SelfDestructSequence()
    {
        // Called when user presses backspace or delete
        StopListeningForInput();

        animationController.PlayBlockEarsSequence();

        yield return new WaitForSeconds(1.0f);
        Explode();
    }

    public void OnPlayerEnteredDoor()
    {
        // Callback fired when Level detects that player has entered an open door
        StopListeningForInput();

        animationController.PlayJumpingSequence();
        allowedToExplode = false; // Player cannot explode while celebrating level win
    }

    private void CheckForCrushedPlayer()
    { 
        // Have any objects landed on the player which can crush us
        BaseTile nNeighbour = GetNeighbour(Direction.N);
        if(nNeighbour != null && nNeighbour.CanCrushPlayer)
        {
            // Did it just land on us (or move sideways)
            // (Updating top-down so it would have just fell on the current step)
            if(nNeighbour.GetCurrentStepMoveDirection() == Direction.S ||
               nNeighbour.GetCurrentStepMoveDirection() == Direction.E ||
               nNeighbour.GetCurrentStepMoveDirection() == Direction.W)
            {
                Explode();
            }
        }
        // TODO player rising on balloon shouldn't crushed by falling object??
    }

#region Overrides
    public override bool Explode()
    {
        if(!allowedToExplode)
            return false;

        // Notify subscribers that player has exploded
        if(playerDiedDelegate != null)
        {
            playerDiedDelegate(level.GetLevelID());
        }

        AudioManager.Instance.PlaySoundAtPosition(SoundType.Explosion, Position);

        // Run base class logic last as it involves destroying this tile
        return base.Explode();
    
    }
#endregion
}
