﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalWall : BaseTile
{
    public override bool IsStableSurface
    {
        get
        {
            return true;
        }
    }

    public override bool IsHardSurface
    {
        get
        {
            return true;
        }
    }
}
