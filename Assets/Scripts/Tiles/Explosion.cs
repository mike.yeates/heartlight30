﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public enum ExplosionType
{
    Shrapnel,
    Normal
};

public class Explosion : BaseTile
{
    private ExplosionSpritesheetAnimation spritesheetAnimation;
    private ExplosionType explosionType;

    private bool tileWasBomb; // Were we a bomb before we became an explosion
    private int stepsSinceSpawn;
    private int stepsUntilDespawn;

#region Properties
    public override bool IsHardSurface
    {
        get
        {
            // Bombs will explode if they land on an existing explosion
            return true;
        }
    }
    public override bool CanExplode
    {
        get
        {
            // Explosions can turn into new explosions
            return true;
        }
    }
#endregion

    void Awake()
    {
        // Called when this object is instantiated
        spritesheetAnimation = GetComponent<ExplosionSpritesheetAnimation>();
    }

    public override void Init(Level level, Vector2Int position)
    {
        base.Init(level, position);

        spritesheetAnimation.Init();

        tileWasBomb = false;
        stepsSinceSpawn = 0;
        stepsUntilDespawn = 0;
    }

    // Called by Level::TriggerExplosionFromPosition
    public void StartExplosion(bool tileWasBomb)
    {
        this.tileWasBomb = tileWasBomb;
        stepsUntilDespawn = 7;
        stepsSinceSpawn = 0;

        spritesheetAnimation.StepAnimation(tileWasBomb, stepsSinceSpawn);

        if(tileWasBomb)
        {
            explosionType = ExplosionType.Shrapnel;
        }
        else
        {
            explosionType = ExplosionType.Normal;
        }
    }

    public void DelayExplosion()
    {
        stepsUntilDespawn = 8;
        stepsSinceSpawn = -1;
        explosionType = ExplosionType.Shrapnel;
    }

    public override void StepPhysics()
    {
        stepsSinceSpawn++;
        stepsUntilDespawn--;

        spritesheetAnimation.StepAnimation(tileWasBomb, stepsSinceSpawn);

        if(stepsUntilDespawn <= 0)
        {
            level.AddEmptyTileAtPosition(Position); // Also destroys this explosion tile
        }

        // Logic to spread explosions to neighbours

        // Explosions only spread if this tile was a bomb (before it became an explosion)
        if(tileWasBomb)
        {
            // On first step after explosion, blow up any neighbouring rocks/balloons/hearts/explosions/grass/brick walls/empty tiles
            if(stepsSinceSpawn == 1)
            {
                explosionType = ExplosionType.Normal;

                Dictionary<Direction, BaseTile> fourNeighbours = GetFourNeighbours();
                foreach(KeyValuePair<Direction,BaseTile> keyValue in fourNeighbours)
                {
                    BaseTile neighbour = keyValue.Value;
                    Direction directionToNeighbour = keyValue.Key;

                    // If neighbour can explode and it isn't a bomb (neighbouring bombs blow up on second step)
                    if(neighbour != null && neighbour.CanExplode && neighbour.TriggersExplosion == false)
                    {
                        // While showing shrapnel (first frame of explosion), explosion can be restarted by neighbouring bombs
                        if(neighbour is Explosion)
                        {
                            Explosion explosionNeighbour = neighbour as Explosion;
                            if(explosionNeighbour.GetExplosionType() == ExplosionType.Shrapnel)
                            {
                                // If we are 1 step in and explosion below is shrapnel, keep it as shrapnel for one more step
                                // When a bomb lands on another bomb, the bottom bomb should stay as shrapnel for 2 steps
                                if(directionToNeighbour == Direction.S)
                                    explosionNeighbour.DelayExplosion();
                            }
                            else
                            {
                                neighbour.Explode();
                            } 
                        }
                        else
                        {
                            neighbour.Explode();
                        }
                    }
                }
            }
            // On second step after explosion, blow up neighbouring bombs
            else if(stepsSinceSpawn == 2)
            {
                Dictionary<Direction, BaseTile> fourNeighbours = GetFourNeighbours();
                foreach(KeyValuePair<Direction,BaseTile> keyValue in fourNeighbours)
                {
                    BaseTile neighbour = keyValue.Value;
                    Direction directionToNeighbour = keyValue.Key;
                
                    if(neighbour != null && neighbour is Bomb)
                    {   
                        // As we sweep the level top/down left/right, tell the neighbour to explode
                        // when it updates so it doesn't step twice in one sweep
                        if(directionToNeighbour == Direction.E ||
                           directionToNeighbour == Direction.S)
                        {
                            neighbour.ScheduleExplode();
                        }
                        else
                        {
                            // Above and to the left can explode right away, as they have already
                            // stepped in this sweep
                            neighbour.Explode();
                        }
                    }
                }
            }
        }
    }

    public int GetStepsSinceSpawn()
    {
        return stepsSinceSpawn;
    }

    public ExplosionType GetExplosionType()
    {
        return explosionType;
    }
}
