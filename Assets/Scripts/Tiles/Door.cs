﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Door : BaseTile
{
    public PlayableDirector openTimeline;

    private bool isOpen;

#region Properties
    public override bool IsStableSurface
    {
        get
        {
            return true;
        }
    }

    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }

    public override bool IsHardSurface
    {
        get
        {
            return true;
        }
    }
#endregion

    public override void Init(Level level, Vector2Int position)
    {
        base.Init(level, position);

        isOpen = false;
        openTimeline.time = 0.0f;
        openTimeline.Evaluate();
    }

    public void OpenDoor()
    {
        if(!isOpen)
        {
            AudioManager.Instance.PlaySoundAtPosition(SoundType.DoorOpen, Position);
            openTimeline.Play();
            isOpen = true;
        } 
    }

    public bool IsOpen()
    {
        return isOpen;
    }
}
