﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : BaseTile
{
#region Properties
    public override bool CanBePushed
    {
        get
        {
            return true;
        }
    }

    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }

    public override bool TriggersExplosion
    {
        get
        {
            return true;
        }
    }

    public override bool IsHardSurface
    {
        get
        {
            return true;
        }
    }

    public override bool ReceivesGravity
    {
        get
        {
            return true;
        }
    }

    public override bool CanCrushPlayer
    {
        get
        {
            return true;
        }
    }
#endregion

}
