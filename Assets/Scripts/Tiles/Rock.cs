﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : BaseTile
{
#region Properties
    public override bool CanBePushed
    {
        get
        {
            return true;
        }
    }

    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }

    public override bool IsHardSurface
    {
        get
        {
            return true;
        }
    }

    public override bool ReceivesGravity
    {
        get
        {
            return true;
        }
    }

    public override bool CanCrushPlayer
    {
        get
        {
            return true;
        }
    }
#endregion

    public override void StepPhysics()
    {
        // Due to top-down physics updates, this can run twice in a single step (ensure that never happens)
        if(currentStepMovementDirection != Direction.None)
            return;

        // Sound effects - if we just landed on a hard surface or hit the bottom of the world
        if(previousStepMovementDirection == Direction.S)
        {
            BaseTile sNeighbour = GetNeighbour(Direction.S);
            if(sNeighbour == null || // Landed on the bottom of the world
            (sNeighbour != null && sNeighbour.IsHardSurface)) // Landed on a hard tile
            {
                AudioManager.Instance.PlaySoundAtPosition(SoundType.Bang, Position);
            }
        }

        base.StepPhysics();
    }
}
