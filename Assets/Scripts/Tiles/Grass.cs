﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : BaseTile
{
    public override bool CanBeEaten
    {
        get
        {
            return true;
        }
    }

    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }

    public override bool IsStableSurface
    {
        get
        {
            return true;
        }
    }

   
}
