﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickWall : BaseTile
{
    public override bool CanExplode
    {
        get
        {
            return true;
        }
    }

    public override bool IsHardSurface
    {
        get
        {
            return true;
        }
    }

}
