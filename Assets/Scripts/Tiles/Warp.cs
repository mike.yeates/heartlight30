﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Warp : BaseTile
{
    public Direction warpDirection;
    public PlayableDirector squashTimeline;

    public override bool IsStableSurface
    {
        get
        {
            return true;
        }
    }

    public override bool IsHardSurface
    {
        get
        {
            return true;
        }
    }

    public override void Init(Level level, Vector2Int position)
    {
        base.Init(level, position);
        
        squashTimeline.time = 0.0f;
        squashTimeline.Evaluate();
    }

    public void Squash()
    {
        squashTimeline.Play();
    }
}
