﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseTile : MonoBehaviour
{
    protected Level level; // The level this tile belongs to (so we can fetch neighbours)

    // Movement history for current step and previous step
    protected Direction currentStepMovementDirection; 
    protected Direction previousStepMovementDirection;

    protected Dictionary<Direction, BaseTile> currentNeighbours;
    protected Dictionary<Direction, BaseTile> previousNeighbours;

    private bool explodeScheduled;

#region Properties
    private Vector2Int _position;
    public virtual Vector2Int Position
    {
        get
        {
            return _position;
        }
        set
        {
            _position = value;
            // Anytime this property changes, update transform so object is rendered at correct pos
            transform.localPosition = new Vector3(value.x, value.y, transform.position.z);
        }
    }

    // Player can eat this tile
    public virtual bool CanBeEaten
    {
        get
        {
            return false;
        }
    }

    // Player can push this tile
    public virtual bool CanBePushed
    {
        get
        {
            return false;
        }
    }

    // Tile can be told to explode by one of its neighbours
    public virtual bool CanExplode
    {
        get
        {
            return false;
        }
    }

    // Can initiate an explosion when it lands on a hard surface
    public virtual bool TriggersExplosion
    {
        get
        {
            return false;
        }
    }

    // Do explorable objects which land on us then explode?
    public virtual bool IsHardSurface
    {
        get
        {
            return false;
        }
    }

    // Can objects rest on us without falling to one side
    public virtual bool IsStableSurface
    {
        get
        {
            return false;
        }
    }

    // Object will fall
    public virtual bool ReceivesGravity
    {
        get
        {
            return false;
        }
    }

    // Object will rise
    public virtual bool ReceivesAntiGravity
    {
        get
        {
            return false;
        }
    }

    // Tile will crush the player if it falls onto the player
    public virtual bool CanCrushPlayer
    {
        get
        {
            return false;
        }
    }
#endregion

    public virtual void Init(Level level, Vector2Int position)
    {
        this.level = level;
        this.Position = position; // Also updates transform

        currentStepMovementDirection = Direction.None;
        previousStepMovementDirection = Direction.None;

        explodeScheduled = false;

        currentNeighbours = new Dictionary<Direction, BaseTile>();
        previousNeighbours = new Dictionary<Direction, BaseTile>();
    }

    public virtual void PreUpdate()
    {
        // Remember previous movement and reset current movement
        previousStepMovementDirection = currentStepMovementDirection;
        currentStepMovementDirection = Direction.None;
    }

    public virtual void StepPhysics()
    {
        if(explodeScheduled)
        {
            // One of our neighbours has instructed us to explode
            Explode();
            return;
        }

        // Prevent multiple movements in a single step
        // (can occur if objects move right or down, as level updates top-down/left-right)
        if(currentStepMovementDirection != Direction.None)
            return;

        currentNeighbours = GetEightNeighbours();

        Direction nextMovementDirection = DetermineMovementDirection();

        if(ShouldExplode(nextMovementDirection))
        {
            // Cancel any movement if we decide to explode
            nextMovementDirection = Direction.None;
        }

        MoveTo(nextMovementDirection);

        previousNeighbours = currentNeighbours;
    }

    public Direction DetermineMovementDirection()
    {
        Direction nextMove = Direction.None;

        if(this.ReceivesGravity)
        {  
            if(CanFallDown())
            {
                nextMove = Direction.S;
                // Stepping physics top-down has naturally resulted in a fall-delay 
                // (i.e objects start falling one step after the space below them becomes free)
            }
            else
            {
                // If we didn't fall, check if we can move left or right
                if(CanFallLeft())
                {
                    nextMove = Direction.W;
                }
                else if(CanFallRight())
                {
                    nextMove = Direction.E;
                }
            } 
        }

        if(this.ReceivesAntiGravity)
        {
            if(CanFloatUp())
            {
                nextMove = Direction.N;
            }
        }

        return nextMove;
    }

    private bool ShouldExplode(Direction nextMove)
    {
        // Logic to start an explosion goes here
        // Logic to spread an explosion to neighbours exists in Explosion.cs

        // If we just moved downwards
        if(previousStepMovementDirection == Direction.S)
        {
            // Are we a bomb
            if(this.TriggersExplosion)
            {
                // Explode if we landed on the bottom of the world
                if(currentNeighbours[Direction.S] == null)
                {
                    return Explode();
                }
                // Explode if we landed on a hard surface
                else if(currentNeighbours[Direction.S].IsHardSurface)
                {
                    // If the hard surface we landed on was a bomb, blow it up on its next update (later this sweep)
                    if(currentNeighbours[Direction.S] is Bomb)
                    {
                        currentNeighbours[Direction.S].ScheduleExplode();
                    }
                   
                    // We (a bomb) should also explode as we landed on a hard surface
                    return Explode(); 
                }
            }   
            // Are we a rock
            else if(this is Rock)
            {
                // If we just landed on a bomb
                if(currentNeighbours[Direction.S]?.TriggersExplosion == true)
                {
                    // Blow up the bomb below us on its next update (later this sweep)
                    currentNeighbours[Direction.S].ScheduleExplode();
                    return true;
                }
            }   
        }
        // If we just moved sideways into the path of a rising balloon (for level 42)
        else if(previousStepMovementDirection == Direction.E ||
                previousStepMovementDirection == Direction.W)
        {
            if(this.TriggersExplosion)
            {
                // If a baloon is below us and it just moved upwards, explode
                if(currentNeighbours[Direction.S]?.ReceivesAntiGravity == true &&
                currentNeighbours[Direction.S].previousStepMovementDirection == Direction.N)
                {
                    return Explode();
                }
            }
        }

        return false;
    }

#region Movement Helpers
    private bool CanFallDown()
    {
        // Get out of here if we're already sitting on the bottom 
        if(Position.y - 1 < 0)
            return false;

        // If there is a balloon below and something on top of us, we can move down (and push down balloon)
        if(currentNeighbours[Direction.S] is Balloon &&
           currentNeighbours[Direction.N] != null && currentNeighbours[Direction.N].ReceivesGravity)
        {
            // Is there room below to push the balloon into
            if(currentNeighbours[Direction.S].GetNeighbour(Direction.S) is Empty)
            {
                // Fall at half speed, as if balloon is fighting back
                if(previousStepMovementDirection != Direction.S)
                {
                    // Push the balloon down and also move ourself down
                    currentNeighbours[Direction.S].MoveTo(Direction.S);
                    return true;
                }
            }
        }

        // If space below, we can fall
        if(currentNeighbours[Direction.S]is Empty)
            return true;
        
        return false;
    }

    private bool CanFallLeft()
    {
        // Get out of here if we're already sitting on the bottom row or left column of the world
        if(Position.y - 1 < 0 || Position.x - 1 < 0)
            return false;

        // Don't move left if we fell on the previous step
        // Creates a 1 step settle time after falling object lands before it moves horizontal
        if(previousStepMovementDirection == Direction.S)
            return false;

        // Don't fall left if sitting on a stable surface
        if(currentNeighbours[Direction.S].IsStableSurface)
            return false;
       
        // An object beside us prevents us moving left
        if((currentNeighbours[Direction.W] is Empty) == false)
            return false;

        // An object below to the left prevents us moving left (we want to fall into this space)
        if((currentNeighbours[Direction.SW] is Empty) == false)
            return false;

        // If if we are sitting on a shrapnel explosion, don't move left
        if(currentNeighbours[Direction.S] is Explosion)
        {
            Explosion southExplosion = currentNeighbours[Direction.S] as Explosion;
            if(southExplosion.GetExplosionType() == ExplosionType.Shrapnel)
                return false;
        }

        // Is there is a row above us
        if(Position.y + 1 < Level.LEVEL_HEIGHT)
        {
            // Don't move into the path of an object which is about to fall
            if(currentNeighbours[Direction.NW] != null &&
               currentNeighbours[Direction.NW].ReceivesGravity &&
               (currentNeighbours[Direction.NW].currentStepMovementDirection == Direction.E ||
                currentNeighbours[Direction.NW].currentStepMovementDirection == Direction.W ||
                currentNeighbours[Direction.NW].currentStepMovementDirection == Direction.S))
                return false;               
        }

        return true;
    }

    private bool CanFallRight()
    {
        // Get out of here if we're already sitting on the bottom row or right column of the world
        if(Position.y - 1 < 0 || Position.x + 1 > Level.LEVEL_WIDTH)
            return false;

        // Get out of here if we're already sitting on the bottom row of the world
        if(Position.y - 1 < 0)
            return false;

        // Don't move right if we fell on the previous step
        // Creates a 1 step settle time after falling object lands before it moves horizontal
        if(previousStepMovementDirection == Direction.S)
            return false;

        // Don't fall right if sitting on a stable surface
        if(currentNeighbours[Direction.S].IsStableSurface)
            return false;

        // An object beside us prevents us moving right
        if((currentNeighbours[Direction.E] is Empty) == false)
            return false;

        // An object below to the right prevents us moving right (we want to fall into this space)
        if((currentNeighbours[Direction.SE] is Empty) == false)
            return false;

         // If if we are sitting on a shrapnel explosion, don't move right
        if(currentNeighbours[Direction.S] is Explosion)
        {
            Explosion southExplosion = currentNeighbours[Direction.S] as Explosion;
            if(southExplosion.GetExplosionType() == ExplosionType.Shrapnel)
                return false;
        }

        // Is there is a row above us
        if(Position.y + 1 < Level.LEVEL_HEIGHT)
        {
            // Don't move into the path of an object which is about to fall
            if(currentNeighbours[Direction.NE] != null &&
               currentNeighbours[Direction.NE].ReceivesGravity &&
               (currentNeighbours[Direction.NE].currentStepMovementDirection == Direction.E ||
                currentNeighbours[Direction.NE].currentStepMovementDirection == Direction.W ||
                currentNeighbours[Direction.NE].currentStepMovementDirection == Direction.S))
                return false;
        }

        return true;
    }

    private bool CanFloatUp()
    {
        // Get out of here if we are already on the top row of the world
        if(Position.y + 1 >= Level.LEVEL_HEIGHT)
            return false;

        // Can always float up if the space above is empty
        if(currentNeighbours[Direction.N] is Empty)
            return true;

        // An object is known to be sitting on us from this point down 

        // If we didn't just get pushed down by something sitting on top
        if(currentStepMovementDirection != Direction.S)
        {
            // Is the object sitting on us one we're allowed to push upwards?
            if(currentNeighbours[Direction.N].ReceivesGravity || currentNeighbours[Direction.N] is Player)
            {
                BaseTile northNorthNeighbour = currentNeighbours[Direction.N].GetNeighbour(Direction.N);
                // We can lift this object. Is there room above it?
                if(northNorthNeighbour != null && northNorthNeighbour is Empty)
                {
                    // Move upwards at half speed
                    if(previousStepMovementDirection != Direction.N)
                    {
                        // Lift object above us
                        currentNeighbours[Direction.N].MoveTo(Direction.N);
                        return true;
                    }
                }
            }
        }
        return false;
    }
#endregion

    public bool MoveTo(Direction direction)
    {
        currentStepMovementDirection = direction;

        if(currentStepMovementDirection != Direction.None)
        {
            Vector2Int targetPosition = Position + MiscHelpers.GetOffsetForDirection(direction);
            if(level.MoveTile(this, targetPosition))
            {
                Position = targetPosition;
                return true;
            }
        }  
        return false;
    }

    public BaseTile GetNeighbour(Direction direction)
    {
        Vector2Int offset = MiscHelpers.GetOffsetForDirection(direction);
        return level.GetTileAtPosition(Position + offset);
    }

    public Dictionary<Direction, BaseTile> GetFourNeighbours()
    {
        Dictionary<Direction, BaseTile> neighbours = new Dictionary<Direction, BaseTile>();
        neighbours.Add(Direction.N, GetNeighbour(Direction.N));
        neighbours.Add(Direction.E, GetNeighbour(Direction.E));
        neighbours.Add(Direction.S, GetNeighbour(Direction.S));
        neighbours.Add(Direction.W, GetNeighbour(Direction.W));
        return neighbours;
    }

    public Dictionary<Direction, BaseTile> GetEightNeighbours()
    {
        Dictionary<Direction, BaseTile> neighbours = new Dictionary<Direction, BaseTile>();
        neighbours.Add(Direction.N, GetNeighbour(Direction.N));
        neighbours.Add(Direction.NE, GetNeighbour(Direction.NE));
        neighbours.Add(Direction.E, GetNeighbour(Direction.E));
        neighbours.Add(Direction.SE, GetNeighbour(Direction.SE));
        neighbours.Add(Direction.S, GetNeighbour(Direction.S));
        neighbours.Add(Direction.SW, GetNeighbour(Direction.SW));
        neighbours.Add(Direction.W, GetNeighbour(Direction.W));
        neighbours.Add(Direction.NW, GetNeighbour(Direction.NW));
        return neighbours;
    }

    public virtual void ScheduleExplode()
    {
        explodeScheduled = true;
    }

    public virtual bool Explode()
    {
        if(level != null && level.TriggerExplosionFromPosition(Position))
        {
            if(TriggersExplosion)
            {
                // Only play audio for the tile which triggered the explosion (not for grass, hearts etc)
                AudioManager.Instance.PlaySoundAtPosition(SoundType.Explosion, Position);
            }
            return true;
        }
        return false;
    }

    public Direction GetCurrentStepMoveDirection()
    {
        return currentStepMovementDirection;
    }
    public Direction GetPreviousStepMoveDirection()
    {
        return previousStepMovementDirection;
    }
    
}
