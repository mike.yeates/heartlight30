﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundType
{
    Applause,
    Bang,
    Collect,
    DoorOpen,
    Explosion,
    HeartFall,
    Warp
};

public enum MusicType
{
    Menu,
    Level
};

public class AudioManager : Singleton<AudioManager>
{
    [Header("Memory Pool")]
    public AudioSource audioSourceTemplate;
    public int maxSimultaneousSounds;
    private AudioSource[] audioSources;

    [Header("Sounds")]
    public AudioClip applauseClip;
    public AudioClip bangClip;
    public AudioClip collectClip;
    public AudioClip doorOpenClip;
    public AudioClip explosionClip;
    public AudioClip[] heartFallClips;
    public AudioClip warpClip;

    [Header("Music")]
    public AudioSource menuMusic;
    public AudioSource levelMusic;

    private bool isSoundEffectsEnabled;
    private float soundEffectsVolume;

#region Init
    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        // Create memory pool of audio sources
        audioSources = new AudioSource[maxSimultaneousSounds];
        for(int i = 0; i < maxSimultaneousSounds; i++)
        {
            AudioSource spawnedAudioSource = Instantiate(audioSourceTemplate, Vector3.zero, Quaternion.identity, this.transform);
            audioSources[i] = spawnedAudioSource;
        }

        isSoundEffectsEnabled = true;
        soundEffectsVolume = 1.0f;
    }

    void OnEnable()
    {
        MenuScreenController.musicVolumeChangedDelegate += OnMusicVolumeChanged;
        MenuScreenController.effectsVolumeChangedDelegate += OnEffectsVolumeChanged;
    }
    void OnDisable()
    {
        MenuScreenController.musicVolumeChangedDelegate -= OnMusicVolumeChanged;
        MenuScreenController.effectsVolumeChangedDelegate -= OnEffectsVolumeChanged;
    }

#endregion

#region Sound Effects
    public void PlaySound(SoundType type)
    {
        // Play sound at centre of level (simulating a 2D sound)
        PlaySoundAtPosition(type, new Vector2Int(Level.LEVEL_WIDTH / 2, Level.LEVEL_HEIGHT / 2));
    }

    public void PlaySoundAtPosition(SoundType type, Vector2Int pos)
    {
        if(!isSoundEffectsEnabled)
            return;

        switch (type)
        {
            case SoundType.Applause:
                PlayClipAtPosition(applauseClip, pos);
                break;
            case SoundType.Bang:
                PlayClipAtPosition(bangClip, pos);
                break;
            case SoundType.Collect:
                PlayClipAtPosition(collectClip, pos);
                break;
            case SoundType.DoorOpen:
                PlayClipAtPosition(doorOpenClip, pos);
                break;
            case SoundType.Explosion:
                PlayClipAtPosition(explosionClip, pos);
                break;
            case SoundType.HeartFall:
                PlayClipAtPosition(GetRandomClipFromCollection(heartFallClips), pos);
                break;
            case SoundType.Warp:
                PlayClipAtPosition(warpClip, pos);
                break;            
        }
    }
#endregion

#region Music
    public void PlayMusicOfType(MusicType musicType)
    {
        AudioSource music = GetMusicForType(musicType);

        if(music != null)
        {
            music.Play();
        }
    }
    public void StopMusicOfType(MusicType musicType)
    {
        AudioSource music = GetMusicForType(musicType);

        if(music != null)
        {
            music.Stop();
        }
    }

    private AudioSource GetMusicForType(MusicType musicType)
    {
        switch(musicType)
        {
            case MusicType.Menu:
                return menuMusic;
            case MusicType.Level:
                return levelMusic;
        }
        return null;
    }
#endregion

#region Events

public void OnMusicVolumeChanged(float volume)
{
    menuMusic.volume = volume;
    levelMusic.volume = volume;
}
public void OnEffectsVolumeChanged(float volume)
{
    soundEffectsVolume = volume;

    if(volume <= 0.0f)
        isSoundEffectsEnabled = false;
    else
        isSoundEffectsEnabled = true;
}

#endregion

#region Helpers
    private void PlayClipAtPosition(AudioClip clip, Vector2Int pos)
    {
        AudioSource audioSource = GetNextFreeAudioSource();
        audioSource.volume = soundEffectsVolume;
        audioSource.transform.position = new Vector3(pos.x, pos.y, 0.0f);
        audioSource.PlayOneShot(clip);
    }
    
    private AudioClip GetRandomClipFromCollection(AudioClip[] clips)
    {
        return clips[Random.Range(0, clips.Length)];
    }

    private AudioSource GetNextFreeAudioSource()
    {
        for(int i = 0; i < maxSimultaneousSounds; i++)
        {
            if(audioSources[i].isPlaying == false)
            {
                return audioSources[i];
            }
        }
        return audioSources[0];
    }

    public void SetSoundEffectsEnabled(bool enabled)
    {
        isSoundEffectsEnabled = enabled;
    }
#endregion

}
