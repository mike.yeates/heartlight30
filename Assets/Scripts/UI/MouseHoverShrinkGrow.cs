﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
public class MouseHoverShrinkGrow : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public float expandMultiplier;
    public float expandSpeed;

    private RectTransform rectTransform;
    private float targetScale; // Uniform
    private float originalScale;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        originalScale = rectTransform.localScale.x;
        targetScale = originalScale;
    }

    void Update()
    {
        // Constantly lerp scale towards target
        float newScale = Mathf.Lerp(rectTransform.localScale.x, targetScale, Time.deltaTime * expandSpeed);
        rectTransform.localScale = new Vector3(newScale, newScale, newScale);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
       Expand();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Shrink();
    }

    public void Expand()
    {
        targetScale = originalScale * expandMultiplier;
    }

    public void Shrink()
    {
        targetScale = originalScale;
    }

}
