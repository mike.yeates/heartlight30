﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class UIAnimation : MonoBehaviour
{
    public PlayableDirector timeline;
    public float timelineSpeedMultiplier = 1.0f;
    public MaskableGraphic[] elementsToFade;
    public float foregroundOpacity = 0.0f; // Controlled via timeline

    private bool isShowing;
    private bool isFadingIn;
    private bool isFadingOut;
 
    public delegate void OnFadeInBeginDelegate();
    public OnFadeInBeginDelegate fadeInBeginDelegate;

    public delegate void OnFadeInCompleteDelegate();
    public OnFadeInCompleteDelegate fadeInCompleteDelegate;

    public delegate void OnFadeOutBeginDelegate();
    public OnFadeOutBeginDelegate fadeOutBeginDelegate;

    public delegate void OnFadeOutCompleteDelegate();
    public OnFadeOutCompleteDelegate fadeOutCompleteDelegate;

    void Start()
    {
        timeline.timeUpdateMode = DirectorUpdateMode.Manual;
        timeline.time = 0.0f;
        SampleTimeline();

        isShowing = false;
        isFadingIn = false;
        isFadingOut = false;

        gameObject.SetActive(false);
    }

    public void ToggleVisibility()
    {
        if(isShowing)
        {
            FadeOut(); 
        }
        else
        {
            FadeIn(); 
        }
    }

    public void FadeIn()
    {
        gameObject.SetActive(true);

        if(fadeInBeginDelegate != null)
            fadeInBeginDelegate();

        isShowing = true;
        isFadingIn = true;
        isFadingOut = false;
    }

    public void FadeOut()
    {  
        if(fadeOutBeginDelegate != null)
            fadeOutBeginDelegate();

        isShowing = false;
        isFadingOut = true;
        isFadingIn = false;
    }

    private void Update()
    {
        if(isFadingIn)
        {
            timeline.time += Time.deltaTime * timelineSpeedMultiplier;
            SampleTimeline();

            if(timeline.time >= timeline.duration)
            {
                isFadingIn = false;
                timeline.time = timeline.duration;

                if(fadeInCompleteDelegate != null)
                    fadeInCompleteDelegate();
            }
        }

        if(isFadingOut)
        {
            timeline.time -= Time.deltaTime * timelineSpeedMultiplier;
            SampleTimeline();

            if(timeline.time <= 0.0f)
            {
                isFadingOut = false;
                timeline.time = 0.0f;

                if(fadeOutCompleteDelegate != null)
                {
                    fadeOutCompleteDelegate();
                }

                gameObject.SetActive(false);
            }
        }
    }

    private void SampleTimeline()
    {
        timeline.Evaluate();
        SetForegroundOpacity(foregroundOpacity);
    }

    private void SetForegroundOpacity(float newOpacity)
    {
        for(int i = 0; i < elementsToFade.Length; i++)
        {
            if(elementsToFade[i] != null)
            {
                elementsToFade[i].color = new Color(elementsToFade[i].color.r,
                                                    elementsToFade[i].color.g,
                                                    elementsToFade[i].color.b,
                                                    newOpacity);
            }
        }
    }
}
