﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Slider))]
public class SliderController : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public MouseHoverShrinkGrow handle;
    public TextMeshProUGUI valueText;
    public string serialiseName; // Key used in PlayerPrefs to save value

    private Slider slider;

    void Awake()
    {
        slider = GetComponent<Slider>();

        if(PlayerPrefs.HasKey(serialiseName))
        {
            // Will fire off OnChanged events so actual volume (and text label) will update
            slider.value = PlayerPrefs.GetFloat(serialiseName);
        }
    }

    void OnDisable()
    {
        PlayerPrefs.SetFloat(serialiseName, slider.value);
    }

    public void OnSelect(BaseEventData eventData) 
    {
        handle.Expand();
    }

    public void OnDeselect (BaseEventData data) 
	{
		handle.Shrink();
	}

    public void OnValueChanged(float value)
    {
        // Map from 0-1 to 0-100 range and throw away decimal component
        valueText.text = (int)(value * 100.0f) + "%";
    }
}
