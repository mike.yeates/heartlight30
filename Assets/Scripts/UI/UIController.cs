﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices; // To call JS functions from C# (update UI text)

public class UIController : MonoBehaviour
{
    // Unity UI (not using in WebGL build)
    [Header("UI Elements")]
    public Text heartsCollectedText;
    public Text totalHeartsText;
    public Text currentLevelText;
    public Text totalLevelsText;

    // Declare external JS functions
    [DllImport("__Internal")]
    private static extern void SetNumHeartsCollected(int numHeartsCollected);
    [DllImport("__Internal")]
    private static extern void SetTotalHearts(int totalHearts);
    [DllImport("__Internal")]
    private static extern void SetCurrentLevel(int currentLevel);
    [DllImport("__Internal")]
    private static extern void SetTotalLevels(int totalLevels);
    [DllImport("__Internal")]
    private static extern void DisableTotalHeartsLabel();

    void OnEnable()
    {
        GameManager.levelChangeDelegate += OnLevelChange;
        Level.heartCollectedDelegate += OnHeartCollected;
        LevelParser.parsingCompleteDelegate += OnLevelParsingComplete;
        Heart.heartExplodedDelegate += OnHeartExploded;
    }

    void OnDisable()
    {
        GameManager.levelChangeDelegate -= OnLevelChange;
        Level.heartCollectedDelegate -= OnHeartCollected;
        LevelParser.parsingCompleteDelegate -= OnLevelParsingComplete;
        Heart.heartExplodedDelegate -= OnHeartExploded;
    }

#region Events
    public void OnHeartCollected(int heartsCollected)
    {
        heartsCollectedText.text = heartsCollected.ToString();

        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SetNumHeartsCollected(heartsCollected);
        }
    }

    public void OnLevelChange(Level newLevel)
    {
        currentLevelText.text = (newLevel.GetLevelID() + 1).ToString();
        totalHeartsText.text = newLevel.GetTotalHearts().ToString();
        heartsCollectedText.text = "0";

        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SetCurrentLevel(newLevel.GetLevelID() + 1);
            SetNumHeartsCollected(0);
            SetTotalHearts(newLevel.GetTotalHearts());
        }
    }

    public void OnLevelParsingComplete(int levelCount)
    {
        totalLevelsText.text = levelCount.ToString();

        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SetTotalLevels(levelCount);
        }
    }

    public void OnHeartExploded()
    {
        // Level is no longer possible to complete
        if(Application.platform == RuntimePlatform.WebGLPlayer)
        {
            DisableTotalHeartsLabel();
        }
    }
#endregion

}
